import os
from tkinter import Label, PhotoImage

from src.constants import Constants

class SuccessLabel(Label):

    def __init__(self, parent):
        super().__init__(parent)
        self.success_img = PhotoImage(file=Constants.IMG_GOAL_SUCCESS).subsample(8, 8)
        self.failed_img = PhotoImage(file=Constants.IMG_GOAL_FAILED).subsample(8, 8)
        self.spinner_imgs = []
        self.current_img = None
        idx = Constants.IMG_SPINNER_OPTS[0]
        while idx <= Constants.IMG_SPINNER_OPTS[1]:
            img_path = os.path.join(Constants.IMG_SPINNER_PATH, str(idx) + "." + Constants.IMG_SPINNER_OPTS[2])
            img = PhotoImage(file=img_path).subsample(2, 2)
            self.spinner_imgs.append(img)
            idx += 1
        self.current_img_index = 0
        self.spin_fct_id = None

    def set_success(self, success):
        if success == True:
            self.current_img = self.success_img
            self.config(image=self.current_img)
            if self.spin_fct_id is not None:
                self.after_cancel(self.spin_fct_id)
                self.spin_fct_id = None
        elif success == False:
            self.current_img = self.failed_img
            self.config(image=self.current_img)
            if self.spin_fct_id is not None:
                self.after_cancel(self.spin_fct_id)
                self.spin_fct_id = None
        elif success == "PENDING":
            self.current_img = self.spinner_imgs[self.current_img_index]
            self.config(image=self.current_img)
            self.spin_fct_id = self.after(100, self.spin)
        else:
            self.current_img = None
            self.config(image="")
            if self.spin_fct_id is not None:
                self.after_cancel(self.spin_fct_id)
                self.spin_fct_id = None

    def spin(self):
        print(str(len(self.spinner_imgs)))
        self.current_img_index = (self.current_img_index + 1) % len(self.spinner_imgs)
        self.current_img = self.spinner_imgs[self.current_img_index]
        self.config(image=self.current_img)
        self.update()
