from tkinter import ttk, N, E, S, W, VERTICAL, Canvas, Scrollbar, Frame

class ScrollingLabelFrame(ttk.LabelFrame):

    def __init__(self, main, padding, text: str):
        super().__init__(main, padding=padding, text=text)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.canvas = Canvas(self)
        self.content_frame = ttk.Frame(self.canvas)
        self.canvas.create_window(0, 0, window=self.content_frame, anchor=N+W)
        self.canvas.grid(row=0, column=0, sticky=N + E + S + W)
        self.scrollbar = ttk.Scrollbar(self, orient=VERTICAL, command=self.canvas.yview)
        self.canvas.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.grid(row=0, column=1, sticky=N+S)
        self.content_frame.bind("<Configure>", self.onscroll)

    def onscroll(self, evt):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))