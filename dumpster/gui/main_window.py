from threading import Thread
from typing import List
from src.constants import Constants
from tkinter import *
from tkinter import ttk
from src import globals
from dumpster.gui.success_label import SuccessLabel
from src.model.task import Task
from src.model.url import Url
from dumpster.gui.scrolling_labelframe import ScrollingLabelFrame
from src.utils import string_util
from src.utils.log import log

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

class MainWindow(Tk):

    def __init__(self, tasks: List[Task], urls: dict):

        Tk.__init__(self)
        globals.set_main_window(self)
        self.tasks = tasks
        self.task = None
        self.urls = urls
        self.title("Privacy Bot")
        self.state("zoomed")
        self['padx'] = 5
        self['pady'] = 5
        self.iconphoto(True, PhotoImage(file=Constants.MAIN_ICON_PATH))
        self.webtech_colors = {
            0: "black",
            1: "orange",
            2: "red"
        }
        self.style = ttk.Style()
        #self.style.configure("TLabelframe", bordercolor="red", borderwidth=2)
        #self.style.configure("TLabelframe.Label")

        # Tasks
        self.tasks_frame = ttk.LabelFrame(self, text="Task", padding=6)
        self.tasks_frame.grid(row=1, column=1, sticky=E + W + N + S)
        self.tasks_cbox = ttk.Combobox(self.tasks_frame, height=4)
        self.tasks_cbox.grid(row=1, column=2)
        task_names = []
        for task in self.tasks:
            task_names.append(task.name)
        self.tasks_cbox['values'] = tuple(task_names)
        self.tasks_cbox.bind("<<ComboboxSelected>>", self.task_selection_change)
    
        # URL Sources
        self.url_sources_frame = ScrollingLabelFrame(self, text="URL Sources", padding=6)
        self.url_sources_frame.grid(row=2, column=1, sticky=E + W + N + S)
        self.url_sources_checkboxes = []
        row = 1
        for filename in self.urls:
            checkbox = ttk.Checkbutton(self.url_sources_frame.content_frame, text=filename, command=self.display_urls_of_sources)
            checkbox.state(['!alternate'])
            checkbox.grid(row=row, column=1, sticky=W)
            row += 1
            self.url_sources_checkboxes.append(checkbox)

        # URLs
        self.urls_frame = ttk.LabelFrame(self, text="URLs", padding=6)
        self.urls_frame.grid(row=3, column=1, sticky=E + W + N + S)
        self.urls_frame.grid_columnconfigure(1, weight=1)
        self.urls_listbox = Listbox(self.urls_frame, selectmode='multiple')
        self.urls_listbox.grid(row=1, column=1, sticky=E+W+N+S)
        self.urls_scrollbar = Scrollbar(self.urls_frame, orient="vertical")
        self.urls_scrollbar.config(command=self.urls_listbox.yview)
        self.urls_scrollbar.grid(row=1, column=2, sticky=N+S)
        self.urls_listbox.configure(yscrollcommand=self.urls_scrollbar.set)

        # Run Options
        self.runoptions_frame = ttk.LabelFrame(self, text="Run", padding=6)
        self.runoptions_frame.grid(row=4, column=1, padx=6, sticky=E + W + N + S)
        self.runheadless_btn = ttk.Checkbutton(self.runoptions_frame, text="Run headless (Beta)")
        self.runheadless_btn.state(['!alternate'])
        self.runheadless_btn.grid(row=1, column=1, sticky=W)
        self.runminimized_btn = ttk.Checkbutton(self.runoptions_frame, text="Run minimized")
        self.runminimized_btn.state(['!alternate'])
        #self.runminimized_btn.state(['selected'])
        self.runminimized_btn.grid(row=2, column=1, sticky=W)
        self.dont_load_imgs_btn = ttk.Checkbutton(self.runoptions_frame, text="Do not load images")
        self.dont_load_imgs_btn.state(['!alternate'])
        self.dont_load_imgs_btn.state(['selected'])
        self.dont_load_imgs_btn.grid(row=3, column=1, sticky=W)

        # Play/Stop Buttons
        self.run_controls_frame = ttk.Frame(self.runoptions_frame, padding=6)
        self.run_controls_frame.grid(row=5, column=1, pady=20, sticky=E + W + N + S)
        self.img_playall = PhotoImage(file=Constants.IMG_PATH_PLAYALL).subsample(5, 5)
        self.img_play = PhotoImage(file=Constants.IMG_PATH_PLAY).subsample(5, 5)
        self.img_pause = PhotoImage(file=Constants.IMG_PATH_PAUSE).subsample(5, 5)
        self.img_stop = PhotoImage(file=Constants.IMG_PATH_STOP).subsample(5, 5)
        self.btn_playall = ttk.Button(self.run_controls_frame, image=self.img_playall, command=lambda:self.run_task(True))
        self.btn_playall.grid(row=1, column=1)
        self.btn_play = ttk.Button(self.run_controls_frame, image=self.img_play, command=self.run_task)
        self.btn_play.grid(row=1, column=2)
        self.btn_pause = ttk.Button(self.run_controls_frame, image=self.img_pause, command=self.pause_task)
        self.btn_pause.grid(row=1, column=3)
        self.btn_pause.config(state=DISABLED)
        self.btn_stop = ttk.Button(self.run_controls_frame, image=self.img_stop, command=self.stop_task)
        self.btn_stop.grid(row=1, column=4)
        self.btn_stop.config(state=DISABLED)
    
        # Current Page
        self.currentpage_frame = ttk.LabelFrame(self, padding=6, text="Current Page")
        self.currentpage_frame.grid(row=1, column=2, padx=6, sticky=E+W+N+S)
        self.currenturl_var = StringVar()
        self.currenturl = ttk.Label(self.currentpage_frame, textvariable=self.currenturl_var)
        self.currenturl.grid(row=1, column=1)

        # Page Goals
        self.goals_frame = ScrollingLabelFrame(self, padding=6, text="Page Goals")
        self.goals_frame.grid(row=2, column=2, padx=6, sticky=E+W+N+S)
        self.goal_elements = []

        # Page Results
        self.pageresults_frame = ScrollingLabelFrame(self, padding=6, text="Page Results")
        self.pageresults_frame.grid(row=3, column=2, padx=6, sticky=E+W+N+S)
        self.pageresults_elements = []
    
        # Console
        self.runlog_frame = ttk.LabelFrame(self, padding=6, text="Console")
        self.runlog_frame.grid(row=4, column=2, padx=6, sticky=E+W)
        self.runlog = Text(self.runlog_frame)
        self.runlog.grid(row=1, column=1)
        self.runlog_scrollbar = Scrollbar(self.runlog_frame, orient="vertical")
        self.runlog_scrollbar.config(command=self.runlog.yview)
        self.runlog_scrollbar.grid(row=1, column=2, sticky=N+S)
        self.runlog.configure(yscrollcommand=self.runlog_scrollbar.set)
        self.runlog.tag_config("green", foreground="green")
        self.runlog.tag_config("yellow", foreground="yellow")
        self.runlog.tag_config("orange", foreground="orange")
        self.runlog.tag_config("red", foreground="red")

        #Page WebTechs
        self.webtechs_frame = ScrollingLabelFrame(self, padding=6, text="Page Technologies")
        self.webtechs_frame.grid(row=1, column=3, padx=6, sticky=E+W+N+S, rowspan=2)
        self.webtech_elements = []

        #Task Report
        self.taskreport_frame = ScrollingLabelFrame(self, padding=6, text="Task Report")
        self.taskreport_frame.grid(row=3, column=3, padx=6, sticky=N+E+S+W, rowspan=2)
        self.taskreport_elements = []

        #row/col configuration
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=3)
        self.grid_rowconfigure(3, weight=3)
        self.grid_rowconfigure(4, weight=1)
        self.grid_columnconfigure(1, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_columnconfigure(3, weight=1)
        labeltext = "ödfsaköljdskksadfksdaffffasdasdasdasdasdasdasdasdasdasdasd"
        self.xxx = ttk.Label(self.taskreport_frame.content_frame, text=labeltext)
        self.xxx.grid(row=1, column=1, sticky=N+E+S+W)
        return
        #Select first Task of Task Combobox
        self.tasks_cbox.current(1)
        self.task_selection_change(None)

###########################################################

    #displays the goals of the selected task
    def display_page_goals(self):
        self.clear_frame(self.goals_frame.content_frame)
        self.goal_elements = []
        row = 1
        for goal in self.task.goals:
            goal_label = ttk.Label(self.goals_frame.content_frame, text=goal)
            goal_label.grid(row=row, column=1, sticky=W + N)
            goal_success_label = SuccessLabel(self.goals_frame.content_frame)
            goal_success_label.grid(row=row, column=2, sticky=W+N)
            self.goal_elements.append([goal_label, goal_success_label])
            row += 1

    def display_log_line(self, s: str, color: str):
        if color:
            # yellow hard to read on white
            if color == "yellow":
                color = "orange"
            self.runlog.insert(END, s + "\n", color)
        else:
            self.runlog.insert(END, s + "\n")
        self.runlog.yview_moveto(1)

    def display_webtechs(self, webtechs: List[dict]):
        row = 1
        for tech in webtechs:
            name = tech["name"]
            category = ""
            if tech["category"] is not None:
                category = " (" + tech["category"] + ")"
            rating = tech["rating"]
            text = (name + category)
            label = ttk.Label(self.webtechs_frame.content_frame, text=text, foreground=self.webtech_colors[rating])
            label.grid(row=row, column=2, sticky=W + N)
            self.webtech_elements.append(label)
            row += 1

    def display_goal_achieved(self, goal: str, achieved: bool):
        for el in self.goal_elements:
            if el[0].cget("text") == goal:
                if achieved == True:
                    el[1].set_success(True)
                elif achieved == False:
                    el[1].set_success(False)
                else:
                    el[1].set_success("PENDING")
                break

    def display_result(self, name: str, result: str, is_file: bool):
        result_name_label = ttk.Label(self.pageresults_frame.content_frame, text=name)
        row = len(self.pageresults_elements) + 1
        result_name_label.grid(row=row, column=1, sticky=W + N)
        result_label_text = None
        if is_file:
            result_label_text = "FILE"
        else:
            result_label_text = result
        result_label = ttk.Label(self.pageresults_frame.content_frame, text=result_label_text)
        result_label.grid(row=row, column=2, sticky=W + N)
        self.pageresults_elements.append([result_name_label, result_label])

    def display_task_report(self):
        self.clear_frame(self.taskreport_frame.content_frame)
        self.taskreport_elements = []
        report = self.task.get_task_report()
        order = ["pages_run", "run_time", "webtech_occurrence", "goal_achievement", "result_acquirement"]
        row = 1
        for key in order:
            if key == "pages_run":
                labeltext = "Pages run"
            elif key == "run_time":
                labeltext = "Runtime"
            elif key == "webtech_occurrence":
                labeltext = "Web Technologies"
            elif key == "goal_achievement":
                labeltext = "Goal achievement"
            elif key == "result_acquirement":
                labeltext = "Results"
            label = ttk.Label(self.taskreport_frame.content_frame, text=labeltext)
            label.grid(row=row, column=1, sticky=W + N)
            if key in ["webtech_occurrence", "goal_achievement", "result_acquirement"]:
                #Multiple Liners (Lists of Tuples)
                lbls = []
                for tupl in report[key]:
                    if key == "webtech_occurrence":
                        category = ""
                        if tupl[0]["category"] is not None:
                            category = " (" + tupl[0]["category"] + ")"
                        lbl0_text = tupl[0]["name"] + category
                        lbl0_fg = self.webtech_colors[tupl[0]["rating"]]
                    else:
                        lbl0_text = tupl[0]
                        lbl0_fg = None
                    lbl0 = ttk.Label(self.taskreport_frame.content_frame, text=lbl0_text, foreground=lbl0_fg)
                    lbl1_text = string_util.float_to_percent_string(tupl[1])
                    lbl0.grid(row=row, column=2, sticky=W + N)
                    lbl1 = ttk.Label(self.taskreport_frame.content_frame, text=lbl1_text)
                    lbl1.grid(row=row, column=3, sticky=W + N)
                    lbls += [lbl0, lbl1]
                    #log("row: " + str(row) + " lbl0: " + lbl0_text + " lbl1: " + lbl1_text)
                    row += 1
                self.taskreport_elements.append([label, lbls])
            else:
                #One Liners
                value_lbl = ttk.Label(self.taskreport_frame.content_frame, text=report[key])
                value_lbl.grid(row=row, column=2, sticky=W + N)
                self.taskreport_elements.append([label, value_lbl])
                row += 1

    def display_urls_of_sources(self):
        self.urls_listbox.delete(0, END)
        url_count = 0
        for checkbox in self.url_sources_checkboxes:
            filename = checkbox.cget("text")
            checked = checkbox.instate(['selected'])
            if checked:
                url_count += len(self.urls[filename])
                for url in self.urls[filename]:
                    self.urls_listbox.insert(END, str(url))
        #self.urls_listbox.select_set(0)
        #self.urls_listbox.event_generate("<<ListboxSelect>>")
        self.urls_frame.configure(text="URLs (" + str(url_count) + ")")

    def get_selected_urls(self) -> List[Url]:
        urls = []
        for i in self.urls_listbox.curselection():
            url = self.urls_listbox.get(i)
            urls.append(Url(url))
        return urls

    def task_selection_change(self, evt):
        #create a new instance of the selected task
        selected_task_name = self.tasks_cbox.get()
        for task in self.tasks:
            if task.name == selected_task_name:
                self.task = task
                break;
        #clear all run related frames
        self.clear_task_run_frames()
        #display goals of task
        self.display_page_goals()

    #creates a new instance of selected task
    def get_new_task_instance(self) -> Task:
        return self.task.__class__()

    def run_task(self, all_urls: bool = False):
        log("running task")
        #Continue if Task is only paused
        if self.task and self.task.is_paused():
            log("resuming paused task")
            thread = Thread(target=self.task.run)
            thread.start()
            return
        if all_urls:
            self.urls_listbox.select_set(0, END)
        urls = self.get_selected_urls()
        if len(urls) == 0:
            return
        self.task = self.get_new_task_instance()
        self.task.set_urls(urls)
        self.clear_task_run_frames() #clear infos of last run
        self.disable_controls()
        run_headless = self.runheadless_btn.instate(['selected'])
        run_minimized = self.runminimized_btn.instate(['selected'])
        dont_load_imgs = self.dont_load_imgs_btn.instate(['selected'])
        #updates window to show all changes
        self.update()
        globals.init_webdriver(run_headless, run_minimized, dont_load_imgs)
        thread = Thread(target=self.task.run) #args=(10,)
        thread.start()

    def stop_task(self):
        log("stopping task after current url")
        self.btn_stop.state(['pressed'])
        self.btn_pause.state(['!pressed'])
        self.task.stop()

    def pause_task(self):
        log("pausing task after current url")
        self.btn_pause.state(['pressed'])
        self.btn_stop.state(['!pressed'])
        self.task.pause()

    #called when task done
    def task_done(self):
        globals.main_window.enable_controls()

    #called when task is resumed after a pause
    def task_resumed(self):
        self.btn_pause.state(['!pressed'])

    def disable_controls(self):
        #disable all controls but pause, stop
        self.tasks_cbox.config(state=DISABLED)
        for cbox in self.url_sources_checkboxes:
            cbox.config(state=DISABLED)
            cbox.state(["!alternate"])
        self.urls_listbox.config(state=DISABLED)
        self.runheadless_btn.config(state=DISABLED)
        self.runheadless_btn.state(["!alternate"])
        self.runminimized_btn.config(state=DISABLED)
        self.runminimized_btn.state(["!alternate"])
        self.dont_load_imgs_btn.config(state=DISABLED)
        self.dont_load_imgs_btn.state(["!alternate"])
        self.btn_play.config(state=DISABLED)
        self.btn_playall.config(state=DISABLED)
        self.btn_pause.config(state=NORMAL)
        self.btn_stop.config(state=NORMAL)

    def enable_controls(self):
        self.tasks_cbox.config(state=NORMAL)
        for cbox in self.url_sources_checkboxes:
            cbox.config(state=NORMAL)
            cbox.state(["!alternate"])
        self.urls_listbox.config(state=NORMAL)
        self.runheadless_btn.config(state=NORMAL)
        self.runheadless_btn.state(["!alternate"])
        self.runminimized_btn.config(state=NORMAL)
        self.runminimized_btn.state(["!alternate"])
        self.dont_load_imgs_btn.config(state=NORMAL)
        self.dont_load_imgs_btn.state(["!alternate"])
        self.btn_play.config(state=NORMAL)
        self.btn_playall.config(state=NORMAL)
        self.btn_stop.config(state=DISABLED)
        self.btn_stop.state(['!pressed'])
        self.btn_pause.config(state=DISABLED)
        self.btn_pause.state(['!pressed'])

    #clear all frames of a page run
    def clear_page_run_frames(self):
        # current url
        self.currenturl_var.set("")
        # page results
        self.clear_frame(self.pageresults_frame.content_frame)
        self.pageresults_elements = []
        # page goals (clears only the page goal achievement true/false)
        for el in self.goal_elements:
            el[1].set_success(None)
        #page technologies
        self.clear_frame(self.webtechs_frame.content_frame)
        self.webtech_elements = []

    #clear all frames modified during a task run
    def clear_task_run_frames(self):
        self.clear_page_run_frames()
        #console
        self.runlog.delete("1.0", END)
        #task report
        self.clear_frame(self.taskreport_frame.content_frame)
        self.taskreport_elements = []

    def clear_frame(self, frame):
        for el in frame.winfo_children():
            el.destroy()