from src.utils import page_util, persona_generator
from src import globals
from src.utils.log import warn, log
from src.model.url import Url

####### DEPRECATED ############
def go():

    current_persona = persona_generator.get_persona("clay231")
    page_urls = [
        Url("youtube.de"),
        Url("amazon.de"),
        Url("spiegel.de")
    ]
    current_page_url = page_urls[2]
    globals.set_persona(current_persona)
    globals.set_current_page_url(current_page_url)

    #### NAVIGATE URL
    log('begin registration', 0)
    log("navigating " + str(current_page_url), 1)
    globals.webdriver.get(str(current_page_url))

    #### CLICK SIGN IN
    log('finding sign in', 1)
    sign_in = page_util.find_sign_in()
    page_util.click_with_expected_url_change(sign_in)

    #### CLICK CREATE ACCOUNT
    log('finding create account', 1)
    create_acc = page_util.find_create_acc()
    page_util.click_with_expected_url_change(create_acc)

    #### FILL OUT REGISTRATION FORM
    log('filling out registration form', 1)
    page_util.fill_registration_form_inputs()
    page_util.check_all_checkboxes()
    page_util.random_select_all_dropdowns()
    if page_util.has_captcha():
        warn("has captcha, trying my best", 1)
        page_util.solve_captcha()
    page_util.hit_enter()

    #### VERIFY ACCOUNT
    '''
    verification_type = page_util.find_verification_type()
    if verification_type == "EMAIL_CODE":
        log("Verify via email code", 1)
        mail_body = mail_util.fetch_verification_mail()
        log("mail_body: " + str(mail_body))
        verification_code = mail_util.parse_verification_code_from_mail(mail_body)
        page_util.paste_verification_code_into_input(verification_code)
        page_util.hit_enter()
    '''

    log('end registration', 0)