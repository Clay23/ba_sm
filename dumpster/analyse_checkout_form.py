def analyse_checkout_form(self, url):
if self.achieved_goals[str(url)]["Find  WooCommerce-Checkout-Form"]:
    find_checkout_form = Find(page_util.find_element_by_tag_and_classes,
                              ["form", ["woocommerce-checkout"], True, False])
    checkout_form: WebElement = find_checkout_form.call()
    # _html = checkout_form.get_attribute('outerHTML')
    # page_html = html.fromstring(_html)
    # _all_css_classes = page_html.xpath("//*[@class]/@class")
    # all_css_classes = set(_all_css_classes)
    # all_els = page_html.xpath("//*")
    all_els = checkout_form.find_elements_by_xpath("//*")
    parsed_els = []
    for el in all_els:
        #should do it with classes/ids and not whole elements because of wp themes etc
        outerHtml = el.get_attribute('outerHTML')
        outerHtml = outerHtml[:(outerHtml.index(">") + 1)]
        parsed_els.append(outerHtml)
    parsed_els_set = set(parsed_els)
    self.add_result("checkout_form_els", parsed_els_set)

################# in task class
els_total = {}
max_occurences = 0
for page_url in self.results:
    if self.results[page_url]["checkout_form_els"]:
        for el in self.results[page_url]["checkout_form_els"]:
            if el not in els_total:
                els_total[el] = 1
            else:
                els_total[el] += 1
            if els_total[el] > max_occurences:
                max_occurences = els_total[el]
els_sorted = []
for i in range(max_occurences + 1):
    for el in els_total:
        if els_total[el] == i:
            els_sorted.append(el + "     (" + str(i) + ")     ")
log(els_sorted)