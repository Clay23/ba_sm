webdriver.get("https://woocommerce.com/showcase/allumbah-pockket-cottages/")
urls = []
for i in range(200):
    link_area_last = page_util.find_element_by_tag_and_classes("div", ["link-area", "last"], False, False)
    site_link = link_area_last.find_element_by_tag_name("a")
    site_link_href = site_link.text
    log(site_link_href)
    if site_link_href not in urls:
        urls.append(site_link_href)
    next_button = Find(page_util.find_element_by_tag_and_classes,
                       ["span", ["button", "edit-showcase-button", "next"], False, False])
    if next_button.call() is None:
        page_util.wait(10)
        if next_button.call() is None:
            log("done")
            break;
    page_util.click(next_button)
    page_util.wait(2)
log(urls)