https://www.restorez.com
Goals achieved:
Find woo version: False
Extract privacy policy: True
Follow random product link: True
Find Woo-Var indicating Product-Page: True
Find add-to-cart button: True
Click add-to-cart button: True
Proceed to checkout: True
Find Woo-Var indicating Checkout-Page: True
Find Woo-Var indicating Guest-Checkout possible: True
Find  WooCommerce checkout form: True
Determine input types: True
Classify inputs as mandatory/optional: True
Extract WooCommerce privacy policy snippet: True
Results:
Privacy Policy: File
Guest Checkout Possible: True
Required Checkout Data: ['FIRSTNAME', 'LASTNAME', 'COUNTRY', 'STREET_HOUSENUMBER', 'CITY', 'STATE', 'ZIP', 'EMAIL', 'TERMS', 'TERMS']
Optional Checkout Data: ['COMPANYNAME', 'APARTMENT', 'PHONE', 'NEWSLETTER', 'CREATE_ACCOUNT', 'SHIP_DIFFERENT_ADDRESS', 'ORDER_NOTES', 'PAYMENT_RELATED', 'PAYMENT_RELATED', 'PAYMENT_RELATED']
Prechecked Checkboxes at Checkout: ['NEWSLETTER']
WooCommerce privacy policy snippet: RestoreZ proudly stands behind our products. If you are not 100% satisfied, RestoreZ will take care of you with our money-back guarantee.Currently, only shipments to the U.S. are available.
