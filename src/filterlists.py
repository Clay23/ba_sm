from src.model.url import Url
from src.utils import file_util

print("reading filter lists from file")
filterlists = file_util.read_filterlists_from_filterlists_folder()

def get_filterlists():
    return filterlists

#Returns max. 1 filterlist (Assuming filterlists do not overlap)
def get_filterlist_for_url(url: Url):
    for filterlist in filterlists:
        if url.pagename_dot_domain in filterlists[filterlist]:
            return filterlist