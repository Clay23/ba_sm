from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from src.model.find import Find
from src.model.task import Task
from src.utils import page_util, persona_generator
from src import globals
from src.model.url import Url
from src.utils.log import warn, log, err, success

class WooCommerce(Task):

    def __init__(self):
        task_name = "WooCommerce"
        super().__init__(task_name)
        self.goals = [
            "Find woo version",
            "Extract privacy policy",
            "Follow random product link",
            "Find Woo-Var indicating Product-Page",
            "Find add-to-cart button",
            "Click add-to-cart button",
            "Proceed to checkout",
            "Find Woo-Var indicating Checkout-Page",
            "Find Woo-Var indicating Guest-Checkout possible",
            "Find  WooCommerce checkout form",
            "Determine input types",
            "Classify inputs as mandatory/optional",
            "Extract WooCommerce privacy policy snippet"
        ]

    #@Override
    def audit_page(self, url: Url):
        webdriver: WebDriver = globals.webdriver

        #### GATHER WOOCOMMERCE INFORMATION
        self.try_goal("Find woo version")
        woo_version = None
        try:
            woo_script = webdriver.find_element_by_xpath("//script[contains(@src, 'woocommerce.min.js?ver=')]")
            woo_version = woo_script.get_attribute("src").split("woocommerce.min.js?ver=")[1]
        except:
            pass
        if woo_version:
            self.goal_achieved("Find woo version", True)
            self.add_result("Woo Version", woo_version)
        else:
            self.goal_achieved("Find woo version", False)

        #all_hrefs = page_util.find_all_local_hrefs()
        direct_children_hrefs = page_util.find_direct_children_hrefs()

        ### EXTRACT PRIVACY POLICY
        self.try_goal("Extract privacy policy")
        pp_goal = False
        pp_href_ends_with = ["privacy-policy", "privacy-policy/"]
        xpath = "//*[(substring(@href, string-length(@href)-string-length('" + pp_href_ends_with[0] + "')+1) = '" + pp_href_ends_with[0] + "')" #endswith workaround
        pp_href_ends_with = pp_href_ends_with [1:]
        for ending in pp_href_ends_with:
            xpath += " or (substring(@href, string-length(@href)-string-length('" + ending + "')+1) = '" + ending + "')"
        xpath += "]"
        pp_url = None
        try:
            pp_link = webdriver.find_element_by_xpath(xpath)
            pp_url = pp_link.get_attribute("href")
        except:
            log("did not find privacy links on page, using default privacy page url")
            default_pp_url = "https://" + url.pagename_dot_domain + "/privacy-policy"
            if page_util.does_url_exist(default_pp_url):
                pp_url = default_pp_url
        if pp_url:
            webdriver.get(pp_url)
            container = None
            try:
                container = webdriver.find_element_by_tag_name("article")
            except:
                try:
                    container = webdriver.find_element_by_tag_name("section")
                except:
                    container = webdriver.find_element_by_tag_name("body")
            if container:
                pp_html = container.get_attribute("outerHTML")
                self.add_file_result("Privacy Policy", pp_html, "html")
                pp_goal = True

        self.goal_achieved("Extract privacy policy", pp_goal)
        #get back to original page
        webdriver.get(str(url))

        ### FOLLOW RANDOM PRODUCT LINK
        self.try_goal("Follow random product link")
        product_link = self.find_random_product_href()
        if not product_link:
            warn("No product link found. Trying direct api endpoints...")
            shop_href = None
            if "shop" in direct_children_hrefs:
                shop_href = direct_children_hrefs["shop"]
            elif "store" in direct_children_hrefs:
                shop_href = direct_children_hrefs["store"]
            if shop_href:
                webdriver.get(shop_href)
                product_link = self.find_random_product_href()
                #try categories in shop
                if not product_link:
                    all_hrefs_in_shop = page_util.find_all_local_hrefs()
                    for href in all_hrefs_in_shop:
                        if "product-category" in href:
                            webdriver.get(href)
                            product_link = self.find_random_product_href()
                            break
        if product_link is not None:
            self.goal_achieved("Follow random product link", True)
            webdriver.get(product_link)
        else:
            self.goal_achieved("Follow random product link", False)
            return
        page_util.wait(1)
        self.try_goal("Find Woo-Var indicating Product-Page")
        product_js_var = page_util.get_global_js_var("wc_single_product_params")
        if (product_js_var):
            self.goal_achieved("Find Woo-Var indicating Product-Page", True)
        else:
            self.goal_achieved("Find Woo-Var indicating Product-Page", False)

        ### ADD TO CART
        self.try_goal("Find add-to-cart button")
        page_util.random_select_all_dropdowns() #necessary for woocommerce variations card forms
        find_add_to_cart = Find(page_util.find_element_by_tag_and_classes, [None, ["single_add_to_cart_button"], False, True, False])
        add_to_cart_btn = find_add_to_cart.call()
        if add_to_cart_btn is None:
            find_add_to_cart = Find(page_util.find_element_by_text, [["add to cart", "add to basket", "in den warenkorb", "in den korb"], False, True, False])
            add_to_cart_btn = find_add_to_cart.call()
        if add_to_cart_btn is None:
            self.goal_achieved("Find add-to-cart button", False)
            return
        self.goal_achieved("Find add-to-cart button", True)
        self.try_goal("Click add-to-cart button")
     #   click_success = page_util.click(find_add_to_cart)
        click_success = page_util.click_js(find_add_to_cart.call())
        if click_success:
            self.goal_achieved("Click add-to-cart button", True)
        else:
            self.goal_achieved("Click add-to-cart button", False)
            return
        page_util.wait(1)
        #wait for ajax call to finish
        page_util.wait_for_jquery_ajax_to_finish(7)

        ### PROCEED TO CHECKOUT
        self.try_goal("Proceed to checkout")
        checkout_link = page_util.find_element_by_partial_attribute_value(None, "href", ["/checkout"], False)
        checkout_url = None
        if checkout_link is None:
            log("did not find checkout links on page, using default checkout url")
            default_checkout_url = "https://" + url.pagename_dot_domain + "/checkout"
            if page_util.does_url_exist(default_checkout_url):
                checkout_url = default_checkout_url
            else:
                self.goal_achieved("Proceed to checkout", False)
                return
        else:
            checkout_url = checkout_link.get_attribute("href")
        log("proceeding to checkout via: " + checkout_url)
        webdriver.get(checkout_url)
        self.goal_achieved("Proceed to checkout", True)
        self.try_goal("Find Woo-Var indicating Checkout-Page")
        checkout_var = page_util.get_global_js_var("wc_checkout_params")
        if checkout_var and ("is_checkout" in checkout_var) and checkout_var["is_checkout"] == "1":
                self.goal_achieved("Find Woo-Var indicating Checkout-Page", True)
        else:
            self.goal_achieved("Find Woo-Var indicating Checkout-Page", False)

        page_util.wait(2)

        ### CHECK IF GUEST CHECKOUT POSSIBLE
        self.try_goal("Find Woo-Var indicating Guest-Checkout possible")
        if checkout_var:
            if "option_guest_checkout" in checkout_var:
                self.goal_achieved("Find Woo-Var indicating Guest-Checkout possible", True)
                guest_checkout = checkout_var["option_guest_checkout"]
                if str(guest_checkout).lower() in ["yes", "true", "1"]:
                    self.add_result("Guest Checkout Possible", True)
                else:
                    self.add_result("Guest Checkout Possible", False)
            else:
                self.goal_achieved("Find Woo-Var indicating Guest-Checkout possible", False)
        else:
            self.goal_achieved("Find Woo-Var indicating Guest-Checkout possible", False)

        ### ANALYSE CHECKOUT FORM
        self.try_goal("Find  WooCommerce checkout form")
        #find_checkout_form = Find(page_util.find_element_by_tag_and_classes, ["form", ["woocommerce-checkout"], True, False])
        #checkout_form = find_checkout_form.call()
        checkout_form: WebElement = None
        try:
            #checkout_form = webdriver.find_element_by_xpath("//form[contains(@class, 'woocommerce-checkout')]//*[@id='customer_details']")
            checkout_form = webdriver.find_element_by_xpath("//form[contains(@class, 'woocommerce-checkout')]")
            self.goal_achieved("Find  WooCommerce checkout form", True)
        except:
            self.goal_achieved("Find  WooCommerce checkout form", False)
            warn("Trying random form element instead...")
            checkout_forms = page_util.filter_non_clickables(webdriver.find_elements_by_xpath("//body//form"))
            if len(checkout_forms) > 0:
                checkout_form = checkout_forms[0]
                warn("...Found a random form to work with")
            if checkout_form is None:
                err("Could not find checkout form")
                return

        self.try_goal("Determine input types")
        self.try_goal("Classify inputs as mandatory/optional")
        at_least_one_input_type_determined = False
        required_inputs = []
        optional_inputs = []
        checked_checkboxes = []

        inputs = page_util.find_form_inputs(checkout_form, False, False)
        for input in inputs:
            label = page_util.find_label_of_form_element(input, False)
            if not label or not page_util.is_clickable(label):
                continue
            #get input type
            input_type = page_util.determine_checkout_form_input_type_by_indicator(label.text)
            if not input_type:
                input_type = page_util.determine_checkout_form_input_type(input)
            if input_type:
                at_least_one_input_type_determined = True
            #if checkbox get checked
            checked = None
            if input.get_attribute("type") == "checkbox":
                checked = input.get_property("checked")
            #determine required
            required = page_util.determine_form_input_required_by_label(label)
            #append results
            input_type_formated = input_type or ("UNKNOWN_FORM_ELEMENT (label: " + label.text + ")")
            if required:
                required_inputs.append(input_type_formated)
            else:
                optional_inputs.append(input_type_formated)
            if checked:
                checked_checkboxes.append(input_type_formated)
        '''
        labels = page_util.filter_non_clickables(checkout_form.find_elements_by_xpath(".//label"))
        for label in labels:
            input_type = page_util.determine_checkout_form_input_type_by_indicator(label.text)
            input = page_util.find_form_element_of_label(label)
            checked = None
            if input:
                if not input_type:
                    #try to find type with input
                    input_type = page_util.determine_checkout_form_input_type(input)
                #if input is a checkbox, check if initially checked
                #ignores js widgets for now
                if input.get_attribute("type") == "checkbox":
                    checked = input.get_property("checked")
            if input_type:
                at_least_one_input_type_determined = True
            required = page_util.determine_form_input_required_by_label(label)
            if input_type:
                input_type_formated = input_type or ("UNKNOWN_FORM_ELEMENT (label: " + label.text + ")")
            if required:
                required_inputs.append(input_type_formated)
            else:
                optional_inputs.append(input_type_formated)
            if checked:
                checked_checkboxes.append(input_type_formated)
        '''

        self.add_result("Required Checkout Data", required_inputs)
        self.add_result("Optional Checkout Data", optional_inputs)
        self.add_result("Prechecked Checkboxes at Checkout", checked_checkboxes)
        self.goal_achieved("Determine input types", at_least_one_input_type_determined) #success if at least one input type determined
        self.goal_achieved("Classify inputs as mandatory/optional", len(required_inputs) > 0) #success if at least one input determined as required

        self.try_goal("Extract WooCommerce privacy policy snippet")
        try:
            pps_el = webdriver.find_element_by_class_name("woocommerce-privacy-policy-text")
            privacy_policy_snippet = pps_el.text
            self.add_result("WooCommerce privacy policy snippet", privacy_policy_snippet)
            self.goal_achieved("Extract WooCommerce privacy policy snippet", True)
        except:
            self.goal_achieved("Extract WooCommerce privacy policy snippet", False)
        #TODO Here is another possibility to click on the privacy policy link if we havent found it earlier

#################################################################################################################################


    def find_random_product_href(self) -> str:
        product_link = None
        product_links = page_util.find_element_by_partial_attribute_value(None, "href", ["/product/"], True)
        if len(product_links) > 0:
            product_link = product_links[0].get_attribute("href")
        if product_link is None:
            #try specific woo link class
            product_links = page_util.find_element_by_tag_and_classes(None, ["woocommerce-LoopProduct-link"], True, False, False)
            if len(product_links) > 0:
                product_link = product_links[0].get_attribute("href")
        return product_link

    #def self.get_woo_urls_from_api():
        #api_url = Constants.BUILT_WITH_LISTS_API + "WooCommerce-Checkout"
        #response = requests.get(api_url)
        #jsonData = json.loads(response.content)
        #return jsonData



