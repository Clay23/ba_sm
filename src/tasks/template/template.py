from src.model.task import Task
from src.model.url import Url

#Task Template
class Template(Task):

    def __init__(self):
        #Set Task Name
        task_name = "Task Template"
        super().__init__(task_name)
        #Set Goals
        self.goals = [
            "goal1",
            "goal2",
            "goal3"
        ]

    def audit_page(self, url: Url):
        #Implement Page Logic here
        pass