from selenium.webdriver.chrome.webdriver import WebDriver

from src.model.task import Task
from src.model.url import Url
from src.utils import page_util
from src.utils.log import log, err
from src import globals

class Ecommerce(Task):

    def __init__(self):
        task_name = "E-Commerce"
        super().__init__(task_name)
        self.goals = [
        ]

    def audit_page(self, url: Url):
        log("Ecommerce task")
        webdriver: WebDriver = globals.webdriver
        product = webdriver.find_element_by_xpath("//*[contains(text(), '€')]")
        page_util.click_xpath("//*[contains(text(), '€')]")
        page_util.random_select_all_dropdowns()
        add_to_basket = webdriver.find_element_by_xpath("//*[contains(text(), 'In die Einkaufstasche')]")
        page_util.click_xpath("//*[contains(text(), 'In die Einkaufstasche')]")
        checkout = webdriver.find_element_by_xpath("//*[contains(text(), 'Zur Kasse')]")
        page_util.click_xpath("//*[contains(text(), 'Zur Kasse')]")

