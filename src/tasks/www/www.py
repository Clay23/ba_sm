from src.model.task import Task
from src.model.url import Url
from src.utils import page_util
from src.utils.log import log, err
from src import globals

class WWW(Task):

    def __init__(self):
        task_name = "www"
        super().__init__(task_name)
        self.goals = [
            "Find Cookie Notice",
        ]

    def audit_page(self, url: Url):
        log("www task")

        self.try_goal("Find Cookie Notice")
        cookie_notice = page_util.find_cookie_notice()
        if cookie_notice:
            self.goal_achieved("Find Cookie Notice", True)
            log("Found Cookie Notice:")
            log(cookie_notice["cookie_notice"])
            self.add_result("Cookie Notice", cookie_notice["cookie_notice"].text)
            links = []
            for link in cookie_notice["links"]:
                links.append(link.text + ": " + link.get_attribute("href"))
                log(link.get_attribute("href"))
            self.add_result("Cookie Notice Links", links)
            buttons = []
            for button in cookie_notice["buttons"]:
                buttons.append(button.text)
                log(button.text)
            self.add_result("Cookie Notice Buttons", buttons)
        else:
            self.goal_achieved("Find Cookie Notice", False)
            err("Could not find Cookie Notice")

        #documents:
        # extract cookie policy
        # extract privacy policy (check tosdr first)
        # extract agb (check tosdr first)

        #Problematic stuff (check specifically for them, in addition to web technology):
        #Widgets (Facebook Likebox, ..)

