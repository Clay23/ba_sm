class Url:

    def __init__(self, raw_url: str):
        raw_url = raw_url.lower()
        self.raw_url = raw_url
        if raw_url.endswith("/"):
            raw_url = raw_url[:-1]
        if raw_url.startswith("http://"):
            self.protocol_prefix = "http://"
            self.pagename_dot_domain = raw_url[7:]
        elif raw_url.startswith("https://"):
            self.protocol_prefix = "https://"
            self.pagename_dot_domain = raw_url[8:]
        else:
            self.protocol_prefix = "http://"
            self.pagename_dot_domain = raw_url

    #returns full url
    def __str__(self):
        return self.protocol_prefix + self.pagename_dot_domain