class Find:

    def __init__(self, function, args):
        self.function = function
        self.args = args

    def call(self):
        return self.function(*self.args)