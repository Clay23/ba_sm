import traceback
from tkinter import Text
from typing import List

from selenium.webdriver.chrome.webdriver import WebDriver

from src.constants import Constants
from src.model.url import Url
from src import globals, web_technologies, task_runner, tosdr, filterlists, tracker
from src.utils import page_util, file_util
from src.utils.log import warn, err, success, log
import time
from src import frontend_controller

class Task:

    def __init__(self, name: str):
        self.urls: List[Url] = []
        self.goals = []
        self.achieved_goals = {}
        self.results = {}
        self.web_techs = {}
        self.pages_run = 0
        self.name = name
        self._running = False
        self._pause = None
        self._stop = None
        self.current_webtech_provider = 0
        self.start_time: float = None
        self.stop_time: float = None

    def set_urls(self, urls: List[Url]):
        self.urls = urls

    #### THIS METHOD NEEDS TO IMPLEMENTED IN THE INHERITING CLASS
    #The audit_page method determines the logic applied to a single url
    def audit_page(self, url: Url):
        pass

    #visits the url and calls audit-method
    def go(self, url: Url):

        self.achieved_goals[str(url)] = {}
        self.results[str(url)] = {}
        self.web_techs[str(url)] = []
        file_util.create_page_dir(self.name, self.start_time, url)
        #globals.main_window.clear_page_run_frames()
        frontend_controller.clear_page_run_frames()
        globals.set_current_page_url(url)
        #globals.main_window.currenturl_var.set(str(url))
        frontend_controller.display_current_url(str(url))
        log("URL: " + str(url), 1)

        try:
            # navigate url
            globals.webdriver.get(str(url))

            # Get Web Techs of Page
            techs = page_util.get_web_technologies_of(url, self.current_webtech_provider % 3)
            retries = 0
            while techs == [] and retries < 2:
                if len(techs) == 0:
                    warn("no web technologies detected")
                techs = page_util.get_web_technologies_of(url, (self.current_webtech_provider + 1) % 3)
                retries += 1
            if techs == []:
                warn("ALL PROVIDER FAILED TO DETECT WEBTECHS")
            self.add_web_technologies(techs)
            self.current_webtech_provider += 1

            '''
            #Get TOSDR infos
            tosdr_info = tosdr.get_tosdr_for_url(url)
            if tosdr_info:
                self.add_result("TOSDR", tosdr_info)

            #TODO produces javascript error ")" because of unescaped quotes or soemthing
            #TODO at the moment only one filterlist is returned for a url, even though it may exist in multiple
            #Check if url is on a filterlist
            lists = {}
            outgoing_hrefs = page_util.find_all_outgoing_hrefs()
            for href in outgoing_hrefs:
                list_name = filterlists.get_filterlist_for_url(Url(href))
                if list_name:
                    if list_name not in lists:
                        lists[list_name] = []
                    lists[list_name].append(href)
            if lists:
                for l in lists:
                    self.add_result(l, lists[l])
            '''

            #call custom audit
            self.audit_page(url)
        except Exception:
            err(traceback.format_exc())
            self.mark_open_goals_as_failed()
        finally:
            page_report = self.get_page_report(url)
            file_util.save_page_report(self.name, self.start_time, url, page_report)
            log("\nPAGE REPORT:\n")
            log(page_report)

    #executes go on each url in self.urls
    def run(self):
        log(f"RUNNING TASK: {self.name.upper()} with {str(len(self.urls))} URLs", 0)
        self._running = True
        self.start_time = time.time()
        file_util.create_task_dir(self.name, self.start_time)
        i = 0
        while i < len(self.urls):
            if self._pause:
                i = self.resume_with
                self.resume_with = None
                self._pause = None
                #globals.main_window.task_resumed()
                frontend_controller.task_resumed()
            if i > 0:
                # new tab for each url
                page_util.new_tab()
            self.go(self.urls[i])
            self.pages_run += 1
            #globals.main_window.display_task_report()
            report = self.get_task_report()
            frontend_controller.display_task_report(report)
            if self._pause and i < (len(self.urls) - 1):
                self.resume_with = i + 1
                return
            elif self._stop:
                self._stop = False
                break
            i += 1
        task_report = self.get_task_report()
        file_util.save_task_report(self.name, self.start_time, task_report)
        log("\nTASK REPORT:\n")
        log(task_report)
        log(f"END TASK: {self.name.upper()}", 0)
        self.stop_time = time.time()
        self._running = False
        #globals.main_window.task_done()
        frontend_controller.task_done()

    #call when beginning to try the goal
    def try_goal(self, goal: str):
        if goal not in self.goals:
            err("unknown goal")
            return
        log("TRYING: " + goal)
        self.goal_achieved(goal, None)

    #sets goal as achieved
    def goal_achieved(self, goal: str, achieved: bool):
        if goal not in self.goals:
            err("unknown goal")
            return
        if achieved == True:
            success(goal + ": " + "DONE")
        elif achieved == False:
            err(goal + ": " + "FAILED")
        page_url = str(globals.current_page_url)
        self.achieved_goals[page_url][goal] = achieved
        #globals.main_window.display_goal_achieved(goal, achieved)
        frontend_controller.display_goal_achieved(goal, achieved)

    #if the goals were not yet set as either True/False, set them to False
    def mark_open_goals_as_failed(self):
        page_url = str(globals.current_page_url)
        for goal in self.achieved_goals[page_url]:
            if self.achieved_goals[page_url][goal] is None:
                self.goal_achieved(goal, False)

    #add a result to the task (p.e. a succesfully extracted information)
    def add_result(self, result_name: str, result):
        page_url = str(globals.current_page_url)
        self.results[page_url][result_name] = result
        #globals.main_window.display_result(result_name, str(result), False)
        frontend_controller.display_result(result_name, str(result), False)

    def add_file_result(self, result_name: str, result, file_extension: str):
        page_url = str(globals.current_page_url)
        self.results[page_url]["file::" + result_name] = result
        file_util.save_page_result(self.name, self.start_time, globals.current_page_url, result_name, result, file_extension)
        #globals.main_window.display_result(result_name, None, True)
        frontend_controller.display_result(result_name, None, True)

    def add_web_technologies(self, techs: List[str]):
        page_url = str(globals.current_page_url)
        for tech in techs:
            category, rating = web_technologies.get_category_and_rating(tech)
            tech_dict = {
                "name": tech,
                "category": category,
                "rating": rating
            }
            #append sorted by rating
            if len(self.web_techs[page_url]) == 0:
                self.web_techs[page_url].append(tech_dict)
            else:
                for i in range(len(self.web_techs[page_url])):
                    if rating >= self.web_techs[page_url][i]["rating"]:
                        self.web_techs[page_url].insert(i, tech_dict)
                        break
                    elif i == len(self.web_techs[page_url]) - 1:
                        self.web_techs[page_url].append(tech_dict)
        #show in gui
        #globals.main_window.display_webtechs(self.web_techs[page_url])
        frontend_controller.display_webtechs(self.web_techs[page_url])

    #return format: [(goal, rate)]
    def get_goal_achievement_rates(self) -> List[tuple]:
        total_achieved_goals = {}
        for goal in self.goals:
            total_achieved_goals[goal] = 0
        for page_url in self.achieved_goals:
            for goal in self.achieved_goals[page_url]:
                if self.achieved_goals[page_url][goal]:
                    total_achieved_goals[goal] += 1
        goal_achievement_rates = []
        for goal in self.goals:
            rate = round(total_achieved_goals[goal] / self.pages_run, 4)
            goal_achievement_rates.append((goal, rate))
        return goal_achievement_rates

    #return format: [(webtech, rate)]
    def get_webtech_occurrence_rates(self) -> List[tuple]:
        total_occurred_webtechs = {}
        all_webtech_objects = {}
        for page_url in self.web_techs:
            for webtech in self.web_techs[page_url]:
                if webtech["name"] not in total_occurred_webtechs:
                    total_occurred_webtechs[webtech["name"]] = 1
                else:
                    total_occurred_webtechs[webtech["name"]] += 1
                all_webtech_objects[webtech["name"]] = webtech
        webtech_occurrence_rates = []
        for webtech in total_occurred_webtechs:
            rate = round(total_occurred_webtechs[webtech] / self.pages_run, 4)
            webtech_obj = all_webtech_objects[webtech]
            if len(webtech_occurrence_rates) == 0:
                webtech_occurrence_rates.append((webtech_obj, rate))
            else:
                for t in webtech_occurrence_rates:
                    if rate >= t[1]:
                        webtech_occurrence_rates.insert(webtech_occurrence_rates.index(t), (webtech_obj, rate))
                        break
                    elif webtech_occurrence_rates.index(t) == len(webtech_occurrence_rates) - 1:
                        webtech_occurrence_rates.append((webtech_obj, rate))
        return webtech_occurrence_rates

    #return format: [(result, rate)]
    def get_result_acquirement_rates(self) -> List[tuple]:
        total_acquired_results = {}
        for page_url in self.results:
            for result_name in self.results[page_url]:
                if result_name not in total_acquired_results:
                    total_acquired_results[result_name] = 1
                else:
                    total_acquired_results[result_name] += 1
        result_acquirement_rates = []
        for result_name in total_acquired_results:
            rate = round(total_acquired_results[result_name] / self.pages_run, 4)
            result_acquirement_rates.append((result_name, rate))
        return result_acquirement_rates

    def get_page_report(self, _url: Url) -> dict:
        #page url
        page_url = str(_url)
        #goals
        goals = []
        for goal in self.goals:
            if goal not in self.achieved_goals[page_url]:
                achieved = None
            else:
                achieved = self.achieved_goals[page_url][goal]
            goals.append((goal, achieved))
        #results
        results = []
        for result_name in self.results[page_url]:
            result = self.results[page_url][result_name]
            if result_name.startswith("file::"):
                result_name = result_name[6:]
                result = "FILE"
            results.append((result_name, result))
        report = {
            "url": page_url,
            "goals": goals,
            "results": results,
            "webtechs": self.web_techs[page_url]
        }
        return report

    #can also be called during runtime after each page (task report snapshot)
    def get_task_report(self) -> dict:
        #pages run
        pages_run = str(self.pages_run) + " / " + str(len(self.urls))
        #calc runtime
        stop_time = None
        if self.stop_time is not None:
            stop_time = self.stop_time
        else:
            stop_time = time.time()
        run_time = time.strftime("%H:%M:%S", time.gmtime(stop_time - self.start_time))
        #Page Technologies in %
        webtech_occurrence = self.get_webtech_occurrence_rates()
        #Goal achievement in %
        goal_achievement = self.get_goal_achievement_rates()
        #Page Results in %
        result_acquirement = self.get_result_acquirement_rates()
        report = {
            "pages_run": pages_run,
            "run_time": run_time,
            "webtech_occurrence": webtech_occurrence,
            "goal_achievement": goal_achievement,
            "result_acquirement": result_acquirement
        }
        return report

    def pause(self):
        self._pause = True
        self._stop = False

    def is_paused(self):
        return self._pause or False

    def stop(self):
        self._stop = True
        self._pause = False

    def is_running(self):
        return self._running


