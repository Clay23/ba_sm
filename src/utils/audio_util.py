import sounddevice
from scipy.io.wavfile import write
from src.constants import Constants
from src.utils.log import log
import speech_recognition

def get_text_from_recorded_audio():
    recognizer = speech_recognition.Recognizer()
    audio_file = speech_recognition.AudioFile(Constants.AUDIO_REC_FILE_PATH)
    with audio_file as source:
        audio = recognizer.record(source)
    try:
        text = recognizer.recognize_google(audio_data=audio, language="de-DE")
        log("recognized text: " + text)
        return text
    except Exception as e:
        print(e)

def record_stereo_mix(seconds: float):
    log("start recording", 2)
    samplerate = 44100
    recording = sounddevice.rec(int(seconds * samplerate), samplerate=samplerate, channels=2)
    sounddevice.wait()
    write(Constants.AUDIO_REC_FILE_PATH, samplerate, recording)
    log("end recording", 2)