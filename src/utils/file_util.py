import csv
import os
import time
from typing import List
from src.constants import Constants
from src.model.url import Url
import json

#reads all .txt and .csv from urls folder
#returns dict: filename -> [urls]
def read_urls_from_urls_folder() -> dict:
    urls = {}
    files = os.listdir(Constants.URLS_PATH)
    for file in files:
        if file.endswith(".txt"):
            urls[file] = read_urls_from_file(file)
        elif file.endswith(".csv"):
            urls[file] = read_urls_from_builtwith_csv(file)
    return urls

#read specifically from builtwith_csv's using the "Location on Site" property as url
def read_urls_from_builtwith_csv(filename: str) -> List[Url]:
    urls: List[str] = []
    with open(os.path.join(Constants.URLS_PATH, filename)) as csv_file:
        woo_bw_metadataexport_csv = csv.reader(csv_file, delimiter=',')
        header = True
        for row in woo_bw_metadataexport_csv:
            if header:
                location_on_site_index = row.index("Location on Site")
                header = False
                continue
            shop_location = row[location_on_site_index]
            if ";" in shop_location:
                multiple_shop_locations = shop_location.split(";")
                for loc in multiple_shop_locations:
                    #dont know why - some urls from builtwith end like this
                    if loc.endswith("*"):
                        loc = loc[:-1]
                    urls.append(Url(loc))
            else:
                if shop_location.endswith("*"):
                    shop_location = shop_location[:-1]
                urls.append(Url(shop_location))
    return urls

#reads each line of a file as an url
def read_urls_from_file(filename: str) -> List[Url]:
    with open(os.path.join(Constants.URLS_PATH, filename), "r") as file:
        url_strings = file.read().split("\n")
    urls: List[str] = []
    for url_str in url_strings:
        if (not url_str.startswith("#")) and (not url_str == ""):
            urls.append(Url(url_str))
    return urls

#saves a result to file at file_results/task_name/page_name as string
def save_page_result(task_name: str, _task_start_time: float, url: Url, result_name: str, result, file_extension: str):
    task_start_time = str(round(_task_start_time, 2))
    with open(os.path.join(Constants.SAVE_PATH, task_name, task_start_time, url.pagename_dot_domain, result_name + "." + file_extension), "w", encoding="utf-8") as file:
        file.write(str(result))

def save_page_report(task_name: str, _task_start_time: float, url: Url, page_report: dict):
    #dump as json
    task_start_time = str(round(_task_start_time, 2))
    with open(os.path.join(Constants.SAVE_PATH, task_name, task_start_time, url.pagename_dot_domain, "PageReport.json"), "w") as file:
        json.dump(page_report, file)

def save_task_report(task_name: str, _task_start_time: float, task_report: dict):
    #dump as json
    task_start_time = str(round(_task_start_time, 2))
    with open(os.path.join(Constants.SAVE_PATH, task_name, task_start_time, "TaskReport.json"), "w") as file:
        json.dump(task_report, file)

def create_task_dir(task_name: str, _task_start_time: float):
    task_start_time = str(round(_task_start_time, 2))
    if not os.path.isdir(os.path.join(Constants.SAVE_PATH, task_name)):
        os.makedirs(os.path.join(Constants.SAVE_PATH, task_name))
    os.makedirs(os.path.join(Constants.SAVE_PATH, task_name, task_start_time))

def create_page_dir(task_name: str, _task_start_time: float, url: Url):
    task_start_time = str(round(_task_start_time, 2))
    os.makedirs(os.path.join(Constants.SAVE_PATH, task_name, task_start_time, url.pagename_dot_domain))


#reads all .txt from filterlists folder
#returns dict: filterlistname -> [urls]
def read_filterlists_from_filterlists_folder() -> dict:
    filterlists = {}
    files = os.listdir(Constants.FILTERLISTS_PATH)
    for filename in files:
        filterlists[filename] = read_filterlist_from_file(filename)
    return filterlists

#reads a filterlist. reads each line of a file as an url string
def read_filterlist_from_file(filename: str) -> List[str]:
    with open(os.path.join(Constants.FILTERLISTS_PATH, filename), "r") as file:
        url_strings = file.read().split("\n")
    urls: List[str] = []
    for url_str in url_strings:
        if (not url_str.startswith("#")) and (not url_str == ""):
            urls.append(url_str)
    return urls