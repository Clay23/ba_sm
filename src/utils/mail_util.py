import email
import imaplib
import time

from src.utils import misc_util
from src import globals

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
from src.utils.log import log, err, success


def bla():
    print("bla")

#returns email_body as string containing html or bare string
def fetch_verification_mail() -> str:
    me = globals.persona
    if misc_util.is_google_company(globals.current_page_url.pagename):
        mail_sender = "google.com"
    else:
        mail_sender = globals.current_page_url.pagename
    con = imaplib.IMAP4_SSL(me.email_imap)
    con.login(me.email, me.email_password)
    cnt = 0
    while cnt < 30:
        con.select('INBOX')
        log("checking from value: " + mail_sender)
        result, data = con.search(None, "FROM", '"{}"'.format(mail_sender), u'UNSEEN')
        # result, urls = con.search(None, "FROM", '"{}"'.format(email_sender))
        msgs = []
        for i in data[0].split():
            typ, data = con.fetch(i, '(RFC822)')
            msgs.append(data)
        log("msgs:")
        log(msgs)
        msg_bodies = []
        for _msg in msgs:
            msg = email.message_from_bytes(_msg[0][1])
            if msg.is_multipart():
                payload = (msg.get_payload(0).get_payload(None, True))
            else:
                payload = (msg.get_payload(None, True))
            msg_bodies.append(payload.decode('utf-8'))
        log("msg bodies:")
        log(msg_bodies)
        if len(msg_bodies) is 1:
            success("verification mail sucessfully fetched.")
            return msg_bodies[0]
        elif len(msg_bodies) > 1:
            err("error fetching verification mail. number of unread verification mails greater 1.")
            return None
        cnt += 1
        time.sleep(0.5)

def parse_verification_code_from_mail(mail_body):
    html_parsed = BeautifulSoup(mail_body)
    all_elements = html_parsed.find_all(True)
    if len(all_elements) > 0:
        #html
        for el in all_elements:
            text = el.text
            if text.isdigit() and int(text) > 1000:
                success("verification code successfully parsed: " + text)
                return text
    else:
        #just text
        lines = mail_body.splitlines()
        for line in lines:
            if line.isdigit() and int(line) > 1000:
                success("verification code successfully parsed: " + str(int(line)))
                return str(int(line))
    err("verification code not parsed")
