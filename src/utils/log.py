import eel

from src import globals
#from src import frontend_controller
from tkinter import END
from selenium.webdriver.remote.webelement import WebElement
from termcolor import colored

def success(val, marking_lvl: int = None):
    log(val, marking_lvl, "green")

def warn(val, marking_lvl: int = None):
    log(val, marking_lvl, "yellow")

def err(val, marking_lvl: int = None):
    log(val, marking_lvl, "red")

def log(val, marking_lvl: int = None, color: str = None):
    if (isinstance(val, str)):
        _print(marking(marking_lvl) + val, color)
    elif (isinstance(val, list)):
        _print(marking(marking_lvl) + "[", color)
        for item in val:
            _print(marking(marking_lvl) + _str(item), color)
        _print(marking(marking_lvl) + "]", color)
    elif (isinstance(val, dict)):
        _print(marking(marking_lvl) + "{", color)
        for key in val:
            _print(marking(marking_lvl) + key + ": " + _str(val[key]), color)
        _print(marking(marking_lvl) + "}", color)
    else:
        _print(marking(marking_lvl) + _str(val), color)

def marking(lvl: int) -> str:
    if lvl is not None:
        if lvl == 0:
            return ">>>>>> "
        elif lvl == 1:
            return ">>> "
        else:
            return "   "
    else:
        return "   "

#pretty prints WebElements
def _str(s):
    if isinstance(s, WebElement):
        inner_html = s.get_attribute("innerHTML")
        outer_html = s.get_attribute("outerHTML")
        if inner_html is None or inner_html == "":
            return outer_html
        formated_str = outer_html[:outer_html.index(">") + 1]
        has_closing_tag = "</" + s.tag_name + ">" in outer_html
        if has_closing_tag:
            formated_str += ("..." + "</" + s.tag_name + ">")
        return formated_str
    else:
        return str(s)

#prints on console, writes to logfile, logs in gui
def _print(s, color: str = None):
    globals.logfile.write(s + "\n")
    if color:
        print(colored(s, color))
    else:
        print(s)
    #globals.main_window.display_log_line(s, color)
    #TODO Go over frontend controller (atm circular dependency problem)
    #frontend_controller.display_log_line(s, color)
    eel.display_log_line(s, color)