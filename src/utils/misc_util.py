from src.utils.string_util import containsEither

def is_google_company(comp):
    google_companies = ["youtube"]
    return containsEither(comp, google_companies)