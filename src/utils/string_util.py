from typing import List


def contains(word1: str, word2: str): #checks if first string contains second string
    if word1 is None or word2 is None:
        return False;
    if word1 == "" or word2 == "":
        return False
    if word2.lower() in word1.lower():
        return True
    return False

def containsAll(searched_term: str, words: List[str]):  # checks if first string contains all other
    if searched_term is None or words is None:
        return False;
    if searched_term == "" or len(words) == 0:
        return False
    for i in range(len(words)):
        if words[i] is None or words[i] == "":
            return False
        if words[i].lower() not in searched_term.lower():
            return False
    return True

def containsEither(searched_term, words: List[str]):  # checks if first string contains all other
    if searched_term is None or words is None:
        return False;
    if searched_term == "" or len(words) == 0:
        return False
    for i in range(len(words)):
        if words[i] is not None and words[i] != "":
            if words[i].lower() in searched_term.lower():
                return True
    return False

def float_to_percent_string(float: float) -> str:
    return str(round(float * 100, 2)) + " %"