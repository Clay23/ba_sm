from faker import Factory
from src.utils import mail_account_generator
from src.model.persona import Persona

faker = Factory.create("de_DE")

def get_random_persona(with_mail: bool = None) -> Persona:
    persona = Persona()
    persona.sex = faker.random_element(["f", "m"])
    if persona.sex == "f":
        persona.first_name = faker.first_name_female()
        persona.last_name = faker.last_name_female()
    else:
        persona.first_name = faker.first_name_male()
        persona.last_name = faker.last_name_male()
    persona.birth = str(faker.date_of_birth())
    #TODO correct adress urls
    persona.street = faker.street()
    persona.houseno = faker.building_number()
    persona.zip = faker.postcode()
    persona.city = faker.city()
    persona.password = faker.password()
    persona.phone = faker.phone_number()
    if with_mail is not None and with_mail:
        mail_account_generator.generate_mail_account_for_persona(persona)
    return persona

def get_persona(id: str) -> Persona:
    persona = Persona()
    if id == "clay232":
        persona.email = "clay232@web.de"
        persona.email_imap = "imap.web.de"
        persona.email_password = "zanderFilet123"
        persona.sex = "m"
        persona.first_name = "Harald"
        persona.last_name = "Fahrradfahren"
        persona.password = "zunderPilz123"
        persona.street = "Augustusweg"
        persona.houseno = "315"
        persona.zip = "01445"
        persona.city = "Radebeul"
        persona.birth = "25.03.1980"
    elif id == "clay231":
        persona.email = "clay231@web.de"
        persona.email_imap = "imap.web.de"
        persona.email_password = "zanderFilet123"
        persona.sex = "m"
        persona.first_name = "Harald"
        persona.last_name = "Fahrradfahren"
        persona.password = "zunderPilz123"
        persona.street = "Augustusweg"
        persona.houseno = "315"
        persona.zip = "01445"
        persona.city = "Radebeul"
        persona.birth = "25.03.1980"
    return persona
