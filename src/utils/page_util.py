import json
import string
import sys
import time
from typing import List

import requests
import selenium
from lxml import html
from selenium.common.exceptions import ElementClickInterceptedException, ElementNotInteractableException, \
    StaleElementReferenceException, NoSuchElementException, TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common import by
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from src.model.find import Find
from src.model.url import Url
from src.utils import audio_util
from src import globals
from src.utils.log import warn, log, success, err
from src.utils.string_util import containsAll, contains, containsEither

def find_sign_in():
    sign_ins: List[WebElement] = find_element_by_text(["sign in", "anmelden", "login"], True, None)
    log('candidates (' + str(len(sign_ins)) + ')')
    for c in sign_ins:
        log(c.get_attribute('outerHTML'))
    sign_ins_with_text = filter_non_clickables(sign_ins)
    log('candidates_with_text: (' + str(len(sign_ins_with_text)) + ')')
    for c in sign_ins_with_text:
        log(c.text)
    if (len(sign_ins_with_text) == 1):
        sign_in = sign_ins_with_text[0]
    else:
        sign_in = find_best_match_for_keywords(sign_ins_with_text, ["sign in", "anmelden"])
    if sign_in is not None:
        success("found signin: " + sign_in.text)
    else:
        err("did not find signin")
    return sign_in

def find_create_acc() -> WebElement:
    create_accs: List[WebElement] = find_element_by_text(["create", "erstellen", "register", "registrieren"], True, None)
    log('candidates (' + str(len(create_accs)) + ')')
    for c in create_accs:
        log(c.get_attribute('outerHTML'))
    create_accs_with_text = filter_non_clickables(create_accs)
    log('candidates_with_text: (' + str(len(create_accs_with_text)) + ')')
    for c in create_accs_with_text:
        log(c.text)
    if len(create_accs_with_text) == 1:
        create_acc = create_accs_with_text[0]
    else:
        create_acc = find_best_match_for_keywords(create_accs_with_text, ["create", "erstellen", "register", "registrieren"])
    if create_acc is not None:
        success("found createacc: " + create_acc.text)
    else:
        err("did not find createacc")
    return create_acc

def fill_registration_form_inputs():
    me = globals.persona
    inputs: List[WebElement] = globals.webdriver.find_elements_by_tag_name("input")
    for input in inputs:
        if input.get_attribute("type") == "text" and input.is_displayed() and input.is_enabled():
            input_type = deduce_input_type_sign_up(input);
            log("deducted input type: " + input_type)
            if input_type == "FULLNAME":
                input.send_keys(me.first_name + " " + me.last_name)
            if input_type == "FIRSTNAME":
                input.send_keys(me.first_name)
            elif input_type == "LASTNAME":
                input.send_keys(me.last_name)
            elif input_type == "EMAIL":
                input.send_keys(me.email)
            elif input_type == "PASSWORD":
                input.send_keys(me.password)

def check_all_checkboxes():
    inputs = globals.webdriver.find_elements_by_tag_name("input")
    for input in inputs:
        if input.get_attribute("type") == "checkbox" and input.is_enabled() and input.is_displayed():
            click(input)

def random_select_all_dropdowns():
    selects: List[WebElement] = globals.webdriver.find_elements_by_tag_name("select")
    for select in selects:
        if select.is_enabled() and select.is_displayed():
            _select = Select(select)
            currently_selected = _select.first_selected_option
            for opt in _select.options:
                if opt != currently_selected:
                    _select.select_by_value(opt.get_property("value"))
                    break;
    #bootstrap selects
    try:
        bootstrap_dd_item: WebElement = globals.webdriver.find_element_by_xpath("//*[contains(@class, 'dropdown-menu')]//*[contains(@class, 'dropdown-item')][1]")
        click_js(bootstrap_dd_item)
    except:
        pass

def hit_enter():
    active_elem = globals.webdriver.switch_to.active_element
    active_elem.send_keys(u'\ue007');

def is_clickable(el: WebElement) -> bool:
    if el.is_enabled() and el.is_displayed():
        aria_readonly = el.get_attribute("aria-readonly") or ""
        aria_hidden = el.get_attribute("aria-hidden") or ""
        aria_role = el.get_attribute("role") or ""
        if not aria_readonly == "true" and not aria_hidden == "true" and not aria_role == "presentation":
            return True
    return False

#TODO remove and replace usages with standard filter method
def filter_non_clickables(arr: List[WebElement]) -> WebElement:
    ret = []
    for el in arr:
        if is_clickable(el):
            ret.append(el)
    return ret

def click_with_expected_url_change(webEl: WebElement):
    url_before = globals.webdriver.current_url
    click(webEl)
    #TODO to be refactored with explicit/implicit wait
    if (url_before == globals.webdriver.current_url):
        warn("no url change. hitting enter")
        hit_enter()
        cnt = 0
        while url_before == globals.webdriver.current_url and cnt < 10:
            warn("waiting for url change... " + str(cnt))
            time.sleep(0.2)
            cnt += 1

def click(find: Find) -> bool:
    el = find.call()
    try:
        el.click()
    except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
        warn("ElementClickInterceptedException/ElementNotInteractableException on wait click on element: \n" + el.get_attribute("outerHTML") + "\n trying click by scrolling there")
        try:
            scroll_to_element(el)
            wait(1)
            el = find.call()
            el.click()
        except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
            try:
                warn(
                    "ElementClickInterceptedException/ElementNotInteractableException on wait click on element: \n" + el.get_attribute(
                        "outerHTML") + "\n trying click with space")
                click_with_space(el)
            except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
                warn(
                    "ElementClickInterceptedException/ElementNotInteractableException on space click on element: \n" + el.get_attribute(
                        "outerHTML") + "\n trying click with raw js")
                el = find.call()
                return click_js(el)
    return True

def click_xpath(xpath) -> bool:
    el = globals.webdriver.find_element_by_xpath(xpath)
    try:
        el.click()
    except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
        warn("ElementClickInterceptedException/ElementNotInteractableException on wait click on element: \n" + el.get_attribute("outerHTML") + "\n trying click by scrolling there")
        try:
            scroll_to_element(el)
            wait(1)
            el = globals.webdriver.find_element_by_xpath(xpath)
            el.click()
        except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
            try:
                warn(
                    "ElementClickInterceptedException/ElementNotInteractableException on wait click on element: \n" + el.get_attribute(
                        "outerHTML") + "\n trying click with space")
                click_with_space(el)
            except (ElementClickInterceptedException, ElementNotInteractableException) as ex:
                warn(
                    "ElementClickInterceptedException/ElementNotInteractableException on space click on element: \n" + el.get_attribute(
                        "outerHTML") + "\n trying click with raw js")
                el = globals.webdriver.find_element_by_xpath(xpath)
                return click_js(el)
    return True

def click_js(el: WebElement) -> bool:
    try:
        globals.webdriver.execute_script("arguments[0].click();", el)
    except:
        err("click_js failed")
        return False
    #TODO what here for ajax or url change not hard 1 sek
    wait(1)
    return True

def click_js_xpath(xpath):
    globals.webdriver.execute_script("$x('" + xpath + "')[0].click();")

def scroll_to_element(el: WebElement):
    globals.webdriver.execute_script("arguments[0].scrollIntoView(true);", el)

def click_with_space(el: WebElement):
    el.send_keys(selenium.webdriver.common.keys.Keys.SPACE)

def click_with_js_executor(el: WebElement):
    globals.webdriver.execute_script("arguments[0].click();", el)

def deduce_input_type_sign_up(input: WebElement):
    aria_label = input.get_attribute("aria-label")
    name = input.get_attribute("name")
    id = input.get_attribute("id")
    clss = input.get_attribute("class")
    placeholder = input.get_attribute("placeholder")
    value = input.get_attribute("value")
    attributes = [aria_label, name, id, clss, placeholder, value];
    log(attributes)
    for attr in attributes:
        if containsAll(attr, ["first", "name"]) or containsAll(attr, ["vor", "name"]):
            return "FIRSTNAME"
        elif containsAll(attr, ["last", "name"]) or containsAll(attr, ["nach", "name"]):
            return "LASTNAME"
        elif contains(attr, "email"):
            return "EMAIL"
        elif contains(attr, "passw"):
            return "PASSWORD"
        elif contains(attr, "name"):
            return "FULLNAME"
    return "UNKNOWN"

def find_input_with_keywords(inputs: List[WebElement], keywords: List[str]):
    for input in inputs:
        aria_label = input.get_attribute("aria-label")
        name = input.get_attribute("name")
        id = input.get_attribute("id")
        clss = input.get_attribute("class")
        placeholder = input.get_attribute("placeholder")
        value = input.get_attribute("value")
        attributes = [aria_label, name, id, clss, placeholder, value];
        for attr in attributes:
            if containsAll(attr, keywords):
                return input

def find_best_match_for_keywords(webElementList, keywordList):
    containing = None
    min_char_diff = sys.maxsize
    identical = None
    for webEl in webElementList:
        text = webEl.text.lower()
        for keyword in keywordList:
            keyword = keyword.lower()
            if text == keyword:
                if identical is None:
                    identical = webEl
            elif contains(text, keyword) or contains(keyword, text):
                char_diff = abs(len(text) - len(keyword))
                if char_diff < min_char_diff:
                    min_char_diff = char_diff
                    containing = webEl
    if identical is not None:
        return identical
    else:
        return containing

def find_verification_type() -> str:
    # verification_code_needed = containsAll(wd.page_source, my_email, "code", "verif")
    # verification_link_needed = containsAll(wd.page_source, my_email, "lick", "link", "verif")
    return "EMAIL_CODE"

def paste_verification_code_into_input(verification_code):
    inputs = globals.webdriver.find_elements_by_tag_name("input")
    log("locating input with keyword 'code'...")
    code_input = find_input_with_keywords(inputs, ["code"])
    if code_input is not None:
        success("code input successfully located")
    code_input.send_keys(verification_code);

def has_captcha() -> bool:
    iframes: List[WebElement] = globals.webdriver.find_elements_by_tag_name("iframe")
    for iframe in iframes:
        if "google.com/recaptcha" in iframe.get_property("src"):
            return True
    return False

def is_element_stale(el: WebElement) -> bool:
    try:
        el.is_displayed()
        return False
    except StaleElementReferenceException:
        return True

def wait(seconds: float):
    log("waiting for " + str(seconds) + " seconds...")
    time.sleep(seconds)
    log("resuming!")

def solve_captcha():
    log("Solving captcha", 1)

    found_checkbox = False
    found_audio_captcha = False
    found_play_audio_challenge = False
    found_solution_input = False

    log("finding checkbox...")
    iframes: List[WebElement] = globals.webdriver.find_elements_by_tag_name("iframe")
    log("iframes: ")
    for iframe in iframes:
        if not is_element_stale(iframe):
            log("iframe src: " + iframe.get_property("src"))
    for iframe in iframes:
        log(is_element_stale(iframe))
        if not is_element_stale(iframe):
            log("trying iframe with src= " + iframe.get_property("src"))
            globals.webdriver.switch_to.frame(iframe)
            log("not staly:")
            checkbox_span = globals.webdriver.find_elements_by_class_name("recaptcha-checkbox")
            log("ceckis: (" + str(len(checkbox_span)) + ")")
            if len(checkbox_span) > 0:
                found_checkbox = True
                success("found checkbox")
                click(checkbox_span[0])
                wait(5)
                break;
        globals.webdriver.switch_to.default_content()
    globals.webdriver.switch_to.default_content()

    if not found_checkbox:
        err("did not find checkbox")
        return

    log("finding audio captcha...")
    iframes: List[WebElement] = globals.webdriver.find_elements_by_tag_name("iframe")
    log("iframes: ")
    for iframe in iframes:
        if not is_element_stale(iframe):
            log("iframe title: " + iframe.get_property("title"))
    for iframe in iframes:
        if not is_element_stale(iframe):
            globals.webdriver.switch_to.frame(iframe)
            log("not staly")
            audio_btn = globals.webdriver.find_elements_by_id("recaptcha-audio-button")
            log("audiooobtn: ")
            log(audio_btn)
            if len(audio_btn) > 0:
                found_audio_captcha = True
                success("found audio captcha")
                click_with_cursor_hover(audio_btn[0])
                wait(5)
                break;
        globals.webdriver.switch_to.default_content()
    globals.webdriver.switch_to.default_content()

    if not found_audio_captcha:
        err("did not find audio captcha")
        return

    log("finding play audio challenge...")
    iframes: List[WebElement] = globals.webdriver.find_elements_by_tag_name("iframe")
    log("iframes: ")
    for iframe in iframes:
        if not is_element_stale(iframe):
            log("iframe title: " + iframe.get_property("title"))
    for iframe in iframes:
        if not is_element_stale(iframe):
            globals.webdriver.switch_to.frame(iframe)
            play_audio_challenge = globals.webdriver.find_elements_by_class_name("rc-audiochallenge-play-button")
            if len(play_audio_challenge) > 0:
                found_play_audio_challenge = True
                success("found play audio challenge")
                play_audio_challenge[0].click()
                break;
        globals.webdriver.switch_to.default_content()
    globals.webdriver.switch_to.default_content()

    if not found_play_audio_challenge:
        err("did not find play audio challenge")
        return

    #record it
    audio_util.record_stereo_mix(5)
    #audio to text
    solution = audio_util.get_text_from_recorded_audio()

    log("finding solution input...")
    iframes: List[WebElement] = globals.webdriver.find_elements_by_tag_name("iframe")
    log("iframes: ")
    for iframe in iframes:
        if not is_element_stale(iframe):
            log("iframe title: " + iframe.get_property("title"))
    for iframe in iframes:
        globals.webdriver.switch_to.frame(iframe)
        if not is_element_stale(iframe):
            input = globals.webdriver.find_elements_by_id("audio-response")
            if len(input) > 0:
                found_solution_input = True
                success("found solution input")
                input[0].send_keys(solution)
                ok_btn = globals.webdriver.find_elements_by_id("recaptcha-verify-button")[0]
                ok_btn.click()
                break;
        globals.webdriver.switch_to.default_content()
    globals.webdriver.switch_to.default_content()

    if not found_solution_input:
        err("did not find solution input")
        return

def new_tab():
    globals.webdriver.execute_script('''window.open("","_blank");''')
    globals.webdriver.switch_to.window(globals.webdriver.window_handles[len(globals.webdriver.window_handles) - 1])

def close_current_tab():
    globals.webdriver.close()
    globals.webdriver.switch_to.window(globals.webdriver.window_handles[len(globals.webdriver.window_handles) - 1])

def wait_until_visible(el: WebElement, secs: float) -> WebElement: #DOES NOT WORK
    wait = WebDriverWait(globals.webdriver, secs)
    wait.until(expected_conditions.visibility_of(el));

def repeating_find(find_function, arg) -> List[WebElement]: #Repeatedly tries to find element
    el = find_function(arg)[0]
    retries = 0
    while is_element_stale(el) and retries < 10:
        warn("retry finding element " + el.tag_name)
        el = find_function(arg)[0]
        retries += 1
    if is_element_stale(el):
        return None
    else:
        return el

def find_element_by_tag_and_classes(tag: str, classes: List[str], multiple_results: bool, should_be_enabled: bool, should_be_displayed: bool) -> WebElement:  #finds the element which was the specified tag and class
    xpath = "//" + (tag or "*") + "[contains(concat(' ',@class,' '),' " + classes[0] + " ')"
    classes = classes[1:]
    for clss in classes:
        #TODO make case insensitive
        xpath += " and (contains(concat(' ',@class,' '),' " + clss + " '))"
    xpath += "]"
    elements = globals.webdriver.find_elements_by_xpath(xpath)
    if should_be_enabled:
        elements = list(filter(WebElement.is_enabled, elements))
    if should_be_displayed:
        elements = list(filter(WebElement.is_displayed, elements))
    if multiple_results:
        return elements
    else:
        if len(elements) == 1:
            return elements[0]
        elif len(elements) > 1:
            return elements[0]
            warn("multiple elements found, taking first")
        else:
            return None

def find_element_by_text(keyword_list: List[str], multiple_results: bool, should_be_enabled: bool, should_be_displayed: bool): #finds the element(s) which have at least one match in keyword list
    xpath = "//body//*[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '" + keyword_list[0] + "')"
    # search 'value' attribute also: (for input with type button/reset/submit)
    xpath += " or (contains(translate(@value, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '" + keyword_list[0] + "'))"
    keyword_list = keyword_list[1:]
    for keyword in keyword_list:
        xpath += " or (contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '" + keyword + "'))"
        # search 'value'-attribute also: (for input with type button/reset/submit)
        xpath += " or (contains(translate(@value, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '" + keyword + "'))"
    xpath += "]"
    elements = globals.webdriver.find_elements_by_xpath(xpath)
    if should_be_enabled:
        elements = list(filter(WebElement.is_enabled, elements))
    if should_be_displayed:
        elements = list(filter(WebElement.is_displayed, elements))
    if multiple_results:
        return elements
    else:
        if len(elements) == 1:
            return elements[0]
        elif len(elements) > 1:
            return elements[0]
            warn("multiple elements found, taking first")
        else:
            return None

def find_element_by_partial_attribute_value(tag, attribute, partial_value_list: List[str], multiple_results: bool) -> WebElement:
    xpath = "//body//" + (tag or "*") + "[contains(translate(@" + attribute + ", 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'" + partial_value_list[0] + "')"
    partial_value_list = partial_value_list[1:]
    for val in partial_value_list:
        xpath += " or contains(translate(@" + attribute + ", 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'" + val + "')"
    xpath += "]"
    if multiple_results:
        return globals.webdriver.find_elements_by_xpath(xpath)
    else:
        try:
            return globals.webdriver.find_element_by_xpath(xpath)
        except NoSuchElementException:
            return None

def find_global_js_vars_with_keywords(keywords):
    find_keywords = """function findKeywords(obj) {
                    var keywords = %s
                    var result = {};
                    return _findKeywords(obj, 0);
                    function _findKeywords(obj, depth) {
                            if (depth > 0) {
                            //right now this means only scope of window
                                return;
                            }
                            for(prop in obj) {
                                try {
                                    if (obj.hasOwnProperty(prop)) {
                                        var includesAnyKeywords = false;
                                        for(var i=0; i<keywords.length; i++) {
                                            if (prop.toLowerCase().includes(keywords[i])) {
                                                includesAnyKeywords = true;
                                            }
                                        }
                                        if (includesAnyKeywords && prop !== "findKeywords" && prop !== "_findKeywords") {
                                            if (!result.hasOwnProperty(prop)) {
                                                result[prop] = obj[prop];
                                            } else {
                                                result[prop + "#"] = obj[prop];
                                            }
                                        } else {
                                            if (typeof obj[prop] == "object" && obj[prop] !== null && !(obj[prop].toString() === "[object Window]")) {
                                                _findWoo(obj[prop], depth + 1);
                                            }
                                        }
                                    }
                                } catch (e) {
                                    //continue on any exception (for exmaple cross-origin exception)
                                    continue;
                                }
                            }
                            return result;
                        }
                    }
                    return findKeywords(window);"""%(str(keywords))
    return globals.webdriver.execute_script(find_keywords)

def get_global_js_var(var):
    try:
        return globals.webdriver.execute_script("return " + var + ";")
    except:
        return None

def wait_for_jquery_ajax_to_finish(max_secs: int):
    log("waiting for ajax to finish")
    jquery_active = globals.webdriver.execute_script("return jQuery.active == 1;")
    while jquery_active and max_secs > 0:
        wait(1)
        max_secs -= 1

#TODO outgoing link = http(s) link questionable
def find_all_outgoing_hrefs() -> List[str]:
    return xpath("//*[@href][substring(@href, 1, 4)='http']/@href")  # startswith http
#CONTINUE: Message: javascript error: missing ) after argument list

#TODO implement with raw xpath (vgl. find_all_outgoing_hrefs)
def find_all_local_hrefs() -> List[str]:
    hrefs = []
    page_url = globals.current_page_url.pagename_dot_domain + "/"
    links = find_element_by_partial_attribute_value(None, "href", [page_url], True)
    for link in links:
        try:
            hrefs.append(link.get_attribute("href"))
        except StaleElementReferenceException:
            warn("stale element exception of href-element")
    return hrefs

#TODO implement with raw xpath (vgl. find_all_outgoing_hrefs)
def find_direct_children_hrefs() -> dict:
    hrefs = []
    page_url = globals.current_page_url.pagename_dot_domain + "/"
    links = find_element_by_partial_attribute_value(None, "href", [page_url], True)
    for link in links:
        try:
            hrefs.append(link.get_attribute("href"))
        except StaleElementReferenceException:
            warn("stale element exception of href-element")
    apis = {}
    for href in hrefs:
        href = href.lower()
        index_after_page_url = href.index(page_url) + len(page_url)
        href_after_page_url = href[index_after_page_url:]
        if len(href_after_page_url) == 0:
            continue
        slashes = href_after_page_url.count("/")
        if slashes == 0:
            apis[href_after_page_url] = href
        elif slashes == 1:
            if href_after_page_url.index("/") == len(href_after_page_url) - 1:
                apis[href_after_page_url[:-1]] = href
    return apis

#returns the web technology of the url, empty array if there are none / an error occurred
def get_web_technologies_of(url: Url, provider: int) -> List[str]:
    log("getting web technologies using provider " + str(provider))
    techs = []
    #Lookup1
    if provider == 0:
        api_url = "https://api.wappalyzer.com/lookup-basic/v1/?url=" + str(url)
        json_response = requests.get(api_url)
        if json_response and json_response.content:
            json_data = json.loads(json_response.content)
            if json_data:
                for t in json_data:
                    techs.append(t["name"])
    #Lookup2
    elif provider == 1:
        api_url = "https://rescan.io/analysis/" + url.pagename_dot_domain
        page = requests.get(api_url)
        page_html = html.fromstring(page.content)
        techs_from_page = page_html.xpath("//table//a[contains(@href, 'technology')]/text()")
        if techs_from_page:
            techs = techs_from_page
    #Lookup2
    elif provider == 2:
        api_url = "https://builtwith.com/" + url.pagename_dot_domain
        excluded_categories = [
            "Language",
            "Frameworks",
            "Mobile",
            "Name Server",
            "Email Hosting Providers",
            "Web Hosting Providers",
            "SSL Certificates",
            "Web Servers",
            "Syndication Techniques",
            "Content Delivery Network",
            "Shipping Providers",
            "CSS Media Queries",
            "Document Standards",
            "Document Encoding"
        ]
        page = requests.get(api_url)
        page_html = html.fromstring(page.content)
        xpath = "//*[contains(@class, 'card-body')]"
        for cat in excluded_categories:
            xpath += ("[not(div[1]//h6/text()='" + cat + "')]")
        xpath += "//h2/*[contains(@href, 'trends.builtwith.com')]/text()"
        techs_from_page = page_html.xpath(xpath)
        if techs_from_page:
            techs = techs_from_page

    return techs

#tests 404
def does_url_exist(url):
    response = requests.head(str(url))
    log(url)
    log(response)
    return response.status_code != 404

#finds all form elements (input, select...) of a form
def find_form_inputs(form: WebElement, should_be_clickable: bool, include_aria_roles: bool):
    #TODO Wie mit composites wie radiogroup umgehen?
    #TODO Allgemein Bedeutung und Anwendung Aria Roles klären
    input_elements = ["input", "select", "textarea"]
    aria_input_roles = ["combobox", "checkbox", "radio", "slider", "spinbutton", "textbox", "listbox"] #tree?
    xpath = ".//*[(self::" + input_elements[0]
    input_elements = input_elements[1:]
    for el in input_elements:
        xpath += (" or self::" + el)
    xpath += ")]"
    if include_aria_roles:
        xpath += " | .//*[(@role='" + aria_input_roles[0] + "'"
        aria_input_roles = aria_input_roles[1:]
        for role in aria_input_roles:
            xpath += " or @role='" + role + "'"
        xpath += ")]"
    els = form.find_elements_by_xpath(xpath)
    if should_be_clickable:
        els = filter_non_clickables(els)
    return els

#finds the label of a form element (label, aria-labelled-by element, ...)
def find_label_of_form_element(el: WebElement, follow_aria_labels: bool) -> WebElement:
    el_id = el.get_attribute("id")
    try:
        return el.find_element_by_xpath("./ancestor::form//*[@for='" + el_id + "']")
    except:
        pass
    if follow_aria_labels:
        label_id = el.get_attribute("aria-labelledby")
        if not label_id:
            label_id = el.get_attribute("aria-describedby")
        if label_id:
            try:
                return globals.webdriver.find_element_by_xpath("./ancestor::form//*[@id='" + label_id + "']")
            except:
                pass
    parent: WebElement = el.find_element_by_xpath("./..")
    if parent.tag_name == "label":
        return parent
    try:
        preceding_sibling: WebElement = el.find_element_by_xpath("./preceding-sibling::label")
        return preceding_sibling
    except:
        pass
    return None

#finds form element of label
def find_form_element_of_label(label: WebElement) -> WebElement:
    #TODO: Aria-Roles
    el_id = label.get_attribute("for")
    if el_id:
        try:
            return globals.webdriver.find_element_by_id(el_id)
        except:
            pass
    try:
        return label.find_element_by_xpath(".//*[self::input or self::select or self::textarea]")
    except:
        pass
    return None

#determines the form element (input, select...) type
def determine_checkout_form_input_type(el: WebElement) -> str:
    name = el.get_attribute("name") or ""
    id = el.get_attribute("id") or ""
    aria_label = el.get_attribute("aria-label") or ""
    placeholder = el.get_attribute("placeholder") or ""
    clss = el.get_attribute("class") or ""
    indicator = "#".join([name, id, aria_label, placeholder, clss]);
    return determine_checkout_form_input_type_by_indicator(indicator)

#checks if form element is required
def determine_form_input_required(el: WebElement) -> bool:
    clss = el.get_attribute("class") or ""
    if contains(clss, "required"):
        return True
    label = find_label_of_form_element(el)
    if label:
        required = determine_form_input_required_by_label(label)
        return required
    return False

#determines the type of a chckout form input
#indicator-string is checked for certain keywords
def determine_checkout_form_input_type_by_indicator(indicator: str) -> str:
    if containsAll(indicator, ["first", "name"]) or containsAll(indicator, ["vor", "name"]):
        return "FIRSTNAME"
    elif containsAll(indicator, ["last", "name"]) or containsAll(indicator, ["nach", "name"]):
        return "LASTNAME"
    elif containsEither(indicator, ["company", "firma", "firmen"]):
        return "COMPANYNAME"
    elif containsEither(indicator, ["country", "land"]):
        return "COUNTRY"
    elif contains(indicator, "street address") or containsAll(indicator, ["street", "house"]) or containsAll(indicator, ["strasse", "haus"]) or containsAll(indicator, ["straße", "haus"]):
        return "STREET_HOUSENUMBER"
    elif containsEither(indicator, ["street", "strasse", "straße"]):
        return "STREET"
    elif containsEither(indicator, ["house", "haus"]):
        return "HOUSENUMBER"
    elif containsEither(indicator, ["city", "town", "stadt", "ort"]):
        return "CITY"
    elif contains(indicator, "state"):
        return "STATE"
    elif contains(indicator, "bundesland"):
        return "BUNDESLAND"
    elif containsEither(indicator, ["zip", "postcode", "postal", "plz", "postleitzahl"]):
        return "ZIP"
    elif containsEither(indicator, ["phone", "telefon", "telephon"]):
        return "PHONE"
    elif contains(indicator, "email"):
        return "EMAIL"
    elif containsEither(indicator, ["apartment", "suite", "unit", "gebäudeteil", "adresszusatz"]):
        return "APARTMENT"
    elif containsEither(indicator, ["terms", "geschäftsbedingungen", "geschaeftsbedingungen"]):
        return "TERMS"
    elif containsAll(indicator, ["privacy", "policy"]) or contains(indicator, "datenschutzerkl"):
        return "PRIVACY_POLICY"
    elif containsAll(indicator, ["ship", "different", "address"]) or containsAll(indicator, ["abweich", "adresse"]):
        return "SHIP_DIFFERENT_ADDRESS"
    elif containsAll(indicator, ["order", "notes"]) or containsEither(indicator, ["notizen", "anmerkungen", "remarks"]):
        return "ORDER_NOTES"
    elif contains(indicator, "newsletter"):
        return "NEWSLETTER"
    elif contains(indicator, "marketing"):
        return "MARKETING"
    elif containsEither(indicator, ["password", "passwort"]):
        return "ACCOUNT_PASSWORD"
    elif containsEither(indicator, ["username", "benutzername"]):
        return "ACCOUNT_USERNAME"
    elif containsAll(indicator, ["create", "account"]):
        return "CREATE_ACCOUNT"
    elif contains(indicator, "first time order"):
        return "FIRST_TIME_ORDER"
    elif containsEither(indicator, ["payment", "paypal", "klarna", "cash on delivery", "cheque", "bank transfer"]):
        return "PAYMENT_RELATED"
    elif contains(indicator, "gdpr"):
        return "GDPR"
    elif contains(indicator, "name"):
        return "FULLNAME"
    warn("could not determine form element type (checkout)")
    return None

#checks the label for "*" and children with classes/title of "required"
def determine_form_input_required_by_label(label: WebElement) -> bool:
    label_text = label.text or ""
    if contains(label_text, "*"):
        return True
    # TODO make case insensitive
    try:
        child_els_tagged_required = label.find_element_by_xpath(
            ".//*[contains(@class, 'required') or contains(@title, 'required')]")
        return True
    except:
        return False

#raw js xpath (for example to get attribute values which are not supported by selenium's get_element_by_xpath)
def xpath(_xpath: str):
    return globals.webdriver.execute_script("$x('" + _xpath + "');")

def find_cookie_notice():
    #TODO minlength of notice text
    #Find element with text "cookie" having length >= 80
    #xpath = "//body//*[not(self::script)][contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), 'cookie')]"
    xpath = (
        "//body//*[not(self::script)][text()]/text()/following-sibling::text()[contains(translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), 'cookie')]/.."
        + " | //body//*[not(self::script)][text()]/text()/self::text()[contains(translate(., 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), 'cookie')]/.."
    )
    potential_cookie_notices = globals.webdriver.find_elements_by_xpath(xpath)
    potential_cookie_notices = list(filter(WebElement.is_displayed, potential_cookie_notices))
    if len(potential_cookie_notices) == 0:
        return None
    #Use Keywordlist to find most likely cookie overlay
    #Keywords with Weight
    cookie_overlay_keywords = [
        ["accept", 2],
        ["ads", 4],
        ["advertisement", 5],
        ["agree", 2],
        ["akzept", 2],
        ["allow", 2],
        ["analyse", 2],
        ["analytics", 3],
        ["analyze", 2],
        ["anbieter", 1],
        ["collect", 1],
        ["consent", 6],
        ["data protection", 6],
        ["data", 2],
        ["daten", 2],
        ["datenschutz", 7],
        ["dienst", 1],
        ["drittanbieter", 7],
        ["dritte", 1],
        ["dritten", 1],
        ["einverstanden", 2],
        ["einwillig", 1],
        ["erfahrung", 1],
        ["experience", 1],
        ["improve", 1],
        ["information", 1],
        ["ip address", 4],
        ["ip adresse", 4],
        ["ip-address", 4],
        ["ip-adresse", 4],
        ["location", 1],
        ["measure", 1],
        ["messung", 1],
        ["nutz", 1],
        ["nutzererfahrung", 7],
        ["nutzung", 3],
        ["opt-in", 7],
        ["opt-out", 7],
        ["partner", 1],
        ["personal", 1],
        ["personalisier", 8],
        ["personalize", 5],
        ["personenbezogene daten", 10],
        ["policy", 4],
        ["privacy policy", 7],
        ["privacy", 4],
        ["process", 1],
        ["sammel", 1],
        ["service", 1],
        ["speicher", 1],
        ["standort", 1],
        ["store", 1],
        ["target audience", 7],
        ["third party", 5],
        ["track", 4],
        ["user experience", 8],
        ["verarbeiten", 1],
        ["verbessern", 1],
        ["werbe-id", 7],
        ["werbung", 3],
        ["widersprechen", 2],
        ["zielgruppe", 3],
        ["zustimm", 1],
    ]
    for el in potential_cookie_notices:
        #if el is a small element containing "cookie" try parent element
        if len(el.text) < 15:
            el = el.find_element_by_xpath("./..")
        score = 0
        highest_score = 0
        cookie_notice: WebElement = None
        for _keyword in cookie_overlay_keywords:
            keyword = _keyword[0]
            weight = _keyword[1]
            eltext = el.text
            if keyword in eltext.lower():
                score += weight
        log(el.text)
        log("--> SCORES " + str(score))
        if score > highest_score:
            highest_score = score
            cookie_notice = el
    if cookie_notice is None:
        return None

    #Get links in notice
    links = cookie_notice.find_elements_by_tag_name("a")
    links = list(filter(WebElement.is_displayed, links))
    #Get Buttons of notice (searching in siblings, cousins, children, grandchildren, etc.)
    button_selector = "[self::button or @role='button' or contains(@class, 'button')]"
    buttons = cookie_notice.find_elements_by_xpath(
        "./following-sibling::*/descendant-or-self::*" + button_selector
        + " | ./preceding-sibling::*/descendant-or-self::*" + button_selector
        + " | .//*" + button_selector
        + " | ./../following-sibling::*//*" + button_selector
        + " | ./../preceding-sibling::*//*" + button_selector)
    buttons = list(filter(WebElement.is_displayed, buttons))
    result = {
        "cookie_notice": cookie_notice,
        "links": links,
        "buttons": buttons
    }
    return result



