from threading import Thread
import eel
from src.model.url import Url
from src.utils.log import log
from src import globals

def run_task(all_urls: bool = False):
    log("running task")
    #Continue if Task is only paused
    if globals.current_task and globals.current_task.is_paused():
        log("resuming paused task")
        thread = Thread(target=globals.current_task.run)
        thread.start()
        return
    if all_urls:
        eel.select_all_urls()
    url_strings = eel.get_selected_urls()()
    urls = []
    for s in url_strings:
        urls.append(Url(s))
    print(urls)
    if len(urls) == 0:
        return
    globals.init_current_task()
    globals.current_task.set_urls(urls)
    eel.clear_task_run_frames() #clear infos of last run
    eel.disable_controls(True)
    run_opts = eel.get_selected_run_options()();
    print(run_opts)
    headless = run_opts["headless"]
    minimized = run_opts["minimized"]
    noimages = run_opts["noimages"]
    #updates window to show all changes
    #self.update()
    globals.init_webdriver(headless, minimized, noimages)
    thread = Thread(target=globals.current_task.run) #args=(10,)
    thread.start()

def stop_task():
    log("stopping task after current url")
    globals.current_task.stop()

def pause_task():
    log("pausing task after current url")
    globals.current_task.pause()

def is_task_running():
    return globals.current_task.is_running()