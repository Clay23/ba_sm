var constants = {
  "MAIN_ICON_PATH": "./assets/logo.gif",
  "IMG_PATH_PLAY": "./assets/play.gif",
  "IMG_PATH_PLAYALL": "./assets/playall.gif",
  "IMG_PATH_PAUSE": "./assets/pause.gif",
  "IMG_PATH_STOP": "./assets/stop.gif",
  "IMG_GOAL_SUCCESS": "./assets/goalsuccess.gif",
  "IMG_GOAL_FAILED": "./assets/goalfailed.gif",
  "IMG_GOAL_PENDING": "./assets/goalpending.gif",

  "WEBTECH_COLOR_CSS": {
    0: "",
    1: "webtech-rating-1",
    2: "webtech-rating-2"
  }
}
