$("#taskdropdown").change(function(evt) {
  eel.task_selection_change(this.value);
});

eel.expose(init_gui);

function init_gui(task_names, url_file_names) {
  for (let name of task_names) {
    $("#taskdropdown").append($("<option />").val(name).text(name))
  }
  for (let name of url_file_names) {
    $("#urlsources").append(
      $("<div class='form-check'><input type='checkbox' id='urlsource" + url_file_names.indexOf(name) + "' class='form-check-input url-sources-checkbox'>" +
        "<label class='form-check-label' for='urlsource" + url_file_names.indexOf(name) + "'>" + name + "</label></div>"));
  }
  $(".url-sources-checkbox").change(async function(evt) {
    let checked_filenames = [];
    $(".url-sources-checkbox").each((index, checkbox) => {
      let checked = $(checkbox).is(':checked');
      if (checked) {
        let labeltext = $("label[for='" + $(checkbox).attr("id") + "']").html();
        checked_filenames.push(labeltext);
      }
    });
    let urls = await eel.get_urls_for_file_sources_selection(checked_filenames)();
    $("#urlscount").html("(" + urls.length + ")");
    $("#urlselect").empty();
    for (let url of urls) {
      $("#urlselect").append($("<option />").val(url).text(url));
    }
  });
  $("#playall").on("click", () => {
    eel.playall();
  });
  $("#play").on("click", () => {
    eel.play();
  });
  $("#pause").on("click", function(evt) {
    if ($(this).hasClass("active")) return;
    $("#stop").removeClass("active");
    $(this).addClass("active");
    eel.pause();
  });
  $("#stop").on("click", function(evt) {
    if ($(this).hasClass("active")) return;
    $("#pause").removeClass("active");
    $(this).addClass("active");
    eel.stop();
  });

  $("#taskdropdown").trigger("change");
}

eel.expose(display_log_line);

function display_log_line(s, color) {
  let color_class = "";
  if (color == "green") color_class = "text-success";
  else if (color == "yellow") color_class = "text-warning";
  else if (color == "red") color_class = "text-danger";
  $("#console").append("<div class='" + color_class + "'>" + s + "</div>");
  $("#console")[0].scrollTop = $("#console")[0].scrollHeight;
}

eel.expose(select_all_urls);

function select_all_urls() {
  console.log("select all urls")
  $("#urlselect > option").attr('selected', true);
}

eel.expose(get_selected_urls);

function get_selected_urls() {
  return $("#urlselect").val();
}

eel.expose(clear_page_run_frames);

function clear_page_run_frames() {
  $("#currenturl").empty();
  $("#pageresults").empty();
  $("#pagegoals").find(".pagegoal-achieved").empty();
  $("#pagewebtechs").empty();
}

eel.expose(clear_task_run_frames);

function clear_task_run_frames() {
  clear_page_run_frames();
  $("#console").empty();
  $("#taskreport").empty();
}

eel.expose(disable_controls);

function disable_controls(disabled) {
  $("#taskdropdown").prop('disabled', disabled);
  $(".url-sources-checkbox").each((index, checkbox) => {
    $(checkbox).prop('disabled', disabled);
  });
  $("#urlselect").prop('disabled', disabled);
  $("#headless").prop('disabled', disabled);
  $("#minimized").prop('disabled', disabled);
  $("#noimages").prop('disabled', disabled);
  $("#playall").prop('disabled', disabled);
  $("#play").prop('disabled', disabled);
  $("#pause").prop('disabled', !disabled);
  $("#stop").prop('disabled', !disabled);
  if (disabled) {
    $("#stop").removeClass("active");
    $("#pause").removeClass("active");
  }
}

eel.expose(get_selected_run_options);

function get_selected_run_options() {
  let headless = $("#headless").is(':checked');
  let minimized = $("#headless").is(':checked');
  let noimages = $("#headless").is(':checked');
  return {
    "headless": headless,
    "minimized": minimized,
    "noimages": noimages
  };
}

eel.expose(display_page_goals);

function display_page_goals(goals) {
  $("#pagegoals").empty();
  for (let g of goals) {
    $("#pagegoals").append("<div class='row'><span class='pagegoal col-10'>" + g + "</span><span class='pagegoal-achieved col-2'></span></div>")
  }
}

eel.expose(display_current_url);

function display_current_url(url) {
  $("#currenturl").html(url);
}

eel.expose(display_webtechs);

function display_webtechs(webtechs) {
  console.log(webtechs)
  for (let tech of webtechs) {
    let name = tech["name"];
    let category = "";
    if (tech["category"] !== null) {
      category = " (" + tech["category"] + ")";
    }
    let rating = tech["rating"];
    let text = name + category;
    let color_class = constants.WEBTECH_COLOR_CSS[rating];
    $("#pagewebtechs").append("<div class='" + color_class + "'>" + text + "</div>");
  }
}

eel.expose(display_result);

function display_result(result_name, result_text) {
  $("#pageresults").append("<div class='row'><span class='result-name col-6'>" + result_name + "</span><span class='result-text col-6'>" + result_text + "</span></div>");
}

eel.expose(display_goal_achieved);

function display_goal_achieved(goal, achieved) {
  let pagegoal_achieved_el = $("#pagegoals").find(".pagegoal").filter(function() {
    return $(this).html() === goal;
  }).siblings(".pagegoal-achieved");
  pagegoal_achieved_el.empty();
  if (achieved === true) {
    pagegoal_achieved_el.append("<img src='" + constants.IMG_GOAL_SUCCESS + "'/>");
  } else if (achieved === false) {
    pagegoal_achieved_el.append("<img src='" + constants.IMG_GOAL_FAILED + "'/>");
  } else {
    pagegoal_achieved_el.append("<div class='spinner-border' />");
  }
}

eel.expose(display_task_report);

function display_task_report(report) {
  reportHtml = "";
  $("#taskreport").empty();
  let order = ["pages_run", "run_time", "webtech_occurrence", "goal_achievement", "result_acquirement"]
  let labeltext = "";
  for (let key of order) {
    if (key == "pages_run") labeltext = "Pages run";
    else if (key == "run_time") labeltext = "Runtime";
    else if (key == "webtech_occurrence") labeltext = "Web Technologies";
    else if (key == "goal_achievement") labeltext = "Goal achievement";
    else if (key == "result_acquirement") labeltext = "Results";
    if (key == "pages_run" || key == "run_time") {
      reportHtml += ("<div class='row'><span class='col-4'>" + labeltext + "</span><span class='col-8'>" + report[key] + "</span></div>");
    } else if (key == "webtech_occurrence") {
      let value_column = "";
      for (tech of report[key]) {
        let name = tech[0].name;
        let category = tech[0].category !== null ? (" (" + tech[0].category + ")") : "";
        let color_class = " " + constants.WEBTECH_COLOR_CSS[tech[0].rating]
        let rate = float_to_percent_string(tech[1]);
        value_column += ("<div class='row'><span class='col-8" + color_class + "'>" + name + category + "</span><span class='col-4'>" + rate + "</span></div>");
      }
      reportHtml += ("<div class='row'><span class='col-4'>" + labeltext + "</span><span class='col-8'>" + value_column + "</span></div>");
    } else {
      let value_column = "";
      for (tech of report[key]) {
        let name = tech[0];
        let rate = float_to_percent_string(tech[1]);
        value_column += ("<div class='row'><span class='col-8'>" + name + "</span><span class='col-4'>" + rate + "</span></div>");
      }
      reportHtml += ("<div class='row'><span class='col-4'>" + labeltext + "</span><span class='col-8'>" + value_column + "</span></div>");
    }
  }
  $("#taskreport").append(reportHtml);
}

eel.expose(task_done);

function task_done() {
  disable_controls(false);
}

eel.expose(task_resumed);

function task_resumed() {
  $("#pause").removeClass("active");
}

function float_to_percent_string(float) {
  return (parseFloat(float) * 100).toFixed(2) + " %";
}
