from src import globals, task_runner
from typing import List
import eel

def init_gui():
    task_names = []
    for task in globals.tasks:
        task_names.append(task.name)
    url_file_names = []
    for filename in globals.urls:
        url_file_names.append(filename)
    eel.init_gui(task_names, url_file_names);
    globals.set_current_task(globals.tasks[0])

def display_log_line(s: str, color: str):
    eel.display_log_line(s, color)

def display_task_report(report: dict):
    eel.display_task_report(report)

def display_goal_achieved(goal: str, achieved: bool):
    eel.display_goal_achieved(goal, achieved)

def display_result(result_name, result, is_file):
    if is_file:
        result_text = "FILE"
    else:
        result_text = result
    eel.display_result(result_name, result_text)

def display_webtechs(webtechs: List[dict]):
    eel.display_webtechs(webtechs)

def clear_page_run_frames():
    eel.clear_page_run_frames()

def display_current_url(url: str):
    eel.display_current_url(url)

def task_resumed():
    eel.task_resumed();

def task_done():
    eel.task_done();

@eel.expose
def task_selection_change(selected_task_name):
    for task in globals.tasks:
        if task.name == selected_task_name:
            globals.set_current_task(task)
            break;
    # clear all run related frames
    eel.clear_task_run_frames()
    # display goals of task
    eel.display_page_goals(globals.current_task.goals)

@eel.expose
def get_urls_for_file_sources_selection(checked_filenames):
    print(checked_filenames)
    urls = []
    for filename in checked_filenames:
       for url in globals.urls[filename]:
        urls.append(str(url))
    return urls

@eel.expose
def playall():
    task_runner.run_task(True)

@eel.expose
def play():
    task_runner.run_task()

@eel.expose
def pause():
    task_runner.pause_task()

@eel.expose
def stop():
    task_runner.stop_task()

@eel.expose
def is_task_running():
    return task_runner.is_task_running();