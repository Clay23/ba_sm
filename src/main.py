from src import globals
from src.tasks.ecommerce.ecommerce import Ecommerce
from src.tasks.template.template import Template
from src.tasks.www.www import WWW
from src.utils import file_util
import eel
from src import frontend_controller

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup
from src.tasks.woo_commerce.woo_commerce import WooCommerce

#TODO

#Add Task Description: What does this task do?


#Requests holen via Chrome DevTools API und so alle thirdparties rausholen (nicht über outgoing links)
#TrackerList benutzen und sagen ob tracker
#Cookies auch holbar über ChromeDevTools (Cookie Dauer, Flash Cookies(?), TracingCookie(von Tracker Webseite))
#Fingerprinting! Googln was es da so fuer loesungen gibt
#Privacy Browser Extensions suchen und integrieren
#Keine page Reports mehr speichern
#Task Report enthält alle page reports, sodas man ein lookup für eine seite machen kan. TR in eine einzige fette Json Datei schreiben! (damit man hinterher daraus nen browsing addon machen kann, das informationen über eine webseite gibt)
#results des tasks auch festlegen/zu beginn anzeigen
#task/page report/webtech als klassen in /data
#Parallelization: Run a Task with multiple threads to increase speed
#webseiten informationen versuchen durch browser addons holen
#only show/save privacy opposed webtechs?

globals.init_logfile()
tasks = [
    Ecommerce(),
    WooCommerce(),
    WWW(),
    Template()
]

urls = file_util.read_urls_from_urls_folder()

globals.set_urls(urls)
globals.set_tasks(tasks)
eel.init("frontend")
frontend_controller.init_gui()
eel.start("main.html") #block=False #while True: eel.sleep()

globals.logfile.close()