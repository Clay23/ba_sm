import os

class Constants:
    ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    CHROME_WEBDRIVER_PATH = os.path.join(ROOT_PATH, "webdrivers", "chromedriver")
    FIREFOX_WEBDRIVER_PATH = os.path.join(ROOT_PATH, "webdrivers", "geckodriver")
    AUDIO_REC_EXT_PATH_CRX = os.path.join(ROOT_PATH, "extensions", "audio_rec_ext.crx")
    AUDIO_REC_EXT_PATH = "C:\\Users\\Steffen\\AppData\\Local\\Google\\Chrome\\User Data\\Default\\Extensions\\kfokdmfpdnokpmpbjhjbcabgligoelgp\\1.1.1_0"
    AUDIO_REC_EXT_OPTIONS_URL = "chrome-extension://kfokdmfpdnokpmpbjhjbcabgligoelgp/options.html"
    DOWNLOAD_DIRECTORY = os.path.join(ROOT_PATH, "download")
    AUDIO_REC_FILE_PATH = os.path.join(ROOT_PATH, "audio_rec", "rec.wav")
    BUILT_WITH_LISTS_API = "https://api.builtwith.com/lists2/api.json?KEY=7d9dffd5-8009-47bc-a0b3-fa61effc1d71&TECH="
    BUILT_WITH_FREE_API = "https://api.builtwith.com/free1/api.json?KEY=7d9dffd5-8009-47bc-a0b3-fa61effc1d71&LOOKUP="
    URLS_PATH = os.path.join(ROOT_PATH, "urls")
    LOG_FILE_PATH = os.path.join(ROOT_PATH, "main.log")
    SAVE_PATH = os.path.join(ROOT_PATH, "save")
    FILTERLISTS_PATH = os.path.join(ROOT_PATH, "filterlists")
