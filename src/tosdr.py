import json

import requests
from src.model.url import Url

#fetches the tosdr data

#TODO fetch single ala "https://tosdr.org/api/1/service/facebook.json"

print("fetching tosdr")
tosdr_json = None
json_response = requests.get("https://tosdr.org/api/1/all.json")
if json_response and json_response.content:
    json_data = json.loads(json_response.content)
    if json_data:
        tosdr_json = json_data
if not tosdr_json:
    print("could not fetch tosdr")

def get_tosdr_for_url(url: Url):
    if tosdr_json and (("tosdr/review/" + url.pagename_dot_domain) in tosdr_json):
        return tosdr_json["tosdr/review/" + url.pagename_dot_domain]
    else:
        return None