from lxml import html
import json
import requests
from src.model.url import Url

#fetches tracker from disconnect.me
#TODO fetch with category from disconnect github

print("fetching tracker from disconnect.me")
tracker = None
blocked_api_url = "https://disconnect.me/trackerprotection/blocked"
blocked_page = requests.get(blocked_api_url)
blocked_page_html = html.fromstring(blocked_page.content)
blocked_tracker = blocked_page_html.xpath("./text()")[0].split("\n\n")
unblocked_api_url = "https://disconnect.me/trackerprotection/unblocked"
unblocked_page = requests.get(unblocked_api_url)
unblocked_page_html = html.fromstring(unblocked_page.content)
unblocked_tracker = unblocked_page_html.xpath("./text()")[0].split("\n\n")
tracker = blocked_tracker + unblocked_tracker
if not blocked_tracker or not unblocked_tracker:
    print("could not fetch trackers")

def is_tracker(url: Url) -> bool:
    return url.pagename_dot_domain in tracker