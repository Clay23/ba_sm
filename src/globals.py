from tkinter import Tk
from typing import List
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.webdriver import WebDriver
from src.constants import Constants
from src.model.persona import Persona
from src.model.task import Task
from src.model.url import Url
from selenium import webdriver as selenium_webdriver

def set_webdriver(_webdriver: WebDriver):
    global webdriver
    webdriver = _webdriver

set_webdriver(None)

def set_tasks(_tasks: List[Task]):
    global tasks
    tasks = _tasks

set_tasks(None)

def set_current_task(_current_task: Task):
    global current_task
    current_task = _current_task

set_current_task(None)

def set_urls(_urls: dict):
    global urls
    urls = _urls

set_urls(None)

def set_persona(_persona: Persona):
    global persona
    persona = _persona

set_persona(None)

def set_current_page_url(_current_page_url: Url):
    global current_page_url
    current_page_url = _current_page_url

set_current_page_url(None)

def set_main_window(_main_window: Tk):
    global main_window
    main_window = _main_window

set_main_window(None)

def init_logfile():
    #### open and clear Logfile
    global logfile
    logfile = open(Constants.LOG_FILE_PATH, "w", encoding="utf-8")
    logfile.write("")
    logfile.close()
    logfile = open(Constants.LOG_FILE_PATH, "a", encoding="utf-8")

def init_webdriver(headless, minimized, dont_load_imgs):
    global webdriver
    if webdriver is not None:
        # kill old webdriver
        webdriver.quit()
        webdriver = None
    chrome_opts: Options = init_webdriver_options(headless, minimized, dont_load_imgs)
    wd = selenium_webdriver.Chrome(executable_path=Constants.CHROME_WEBDRIVER_PATH, options=chrome_opts)
    set_webdriver(wd)
    if minimized:
        wd.minimize_window()

def init_webdriver_options(headless: bool, minimized: bool, dont_load_imgs: bool) -> Options:
    chrome_opts = Options()
    chrome_opts.add_argument('start-maximized')
    chrome_opts.add_argument("window-size=1920,1080")
    # chrome_opts.add_argument("disable-infobars")
    # chrome_opts.add_argument("--disable-extensions")
    if headless:
        chrome_opts.add_argument("--headless")
        # headless chrome fixes
        chrome_opts.add_argument("--disable-gpu")
        chrome_opts.add_argument("--no-sandbox")
        chrome_opts.add_argument("--proxy-server='direct://'");
        chrome_opts.add_argument("--proxy-bypass-list=*");
        # changing user-agent to hide that we are going headless
        chrome_opts.add_argument(
            "user-agent=Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36")
    # hides chrome is controlled by automated software... and gives higher google captcha score
    chrome_opts.add_experimental_option("excludeSwitches", ["enable-automation"])
    chrome_opts.add_experimental_option("useAutomationExtension", False)
    # audio rec extension
    # chrome_opts.add_extension(Const.AUDIO_REC_EXT_PATH_CRX)
    # Pretend to be Google bot (causes cloudflare to block you)
    # chrome_opts.add_argument("user-agent=Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
    # automatic download
    prefs = {
        "profile.default_content_setting_values.automatic_downloads": 1,
        "download.default_directory": Constants.DOWNLOAD_DIRECTORY
    }
    chrome_opts.add_experimental_option("prefs", prefs)
    if dont_load_imgs:
        chrome_opts.add_argument("--blink-settings=imagesEnabled=false")
    return chrome_opts

def init_current_task():
    #inits a new task instance
    global current_task
    set_current_task(current_task.__class__())